import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
declare var $: any;
import { ConnectionService } from 'ng-connection-service';  
import localeIt from '@angular/common/locales/it';
import localeEnGb from '@angular/common/locales/en-GB';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'sewe';
  isConnected = true; 
  message;
  constructor(private connectionService: ConnectionService) {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (!this.isConnected) {
        $.toast({
          heading: 'Attention',
          text: "Verifier votre connexion internet. Si vous déjà etes connectez alors verifier que vous avez une bonne connexion",
          position: 'top-right',
          loaderBg:'#f70909',
          icon: 'error',
          hideAfter: 30000, 
          stack: 6
        });
      }
    })
  }
//   ngOnInit() {
//     registerLocaleData(localeIt, 'it-IT');
//     registerLocaleData(localeEnGb, 'en-GB');
// }


}

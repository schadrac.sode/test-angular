import { ModelInterface } from "./model.interface";
import { User } from "./user.model";
import { Offer } from "./offer.model";

export class Subscription  implements  ModelInterface {

    id:number;
    user:User;
    offer:Offer; 
    refTransaction:string;
    is_acive:boolean;
    expiredAt:string;
    deleted:boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
    this.user=attrs.user;
    this.offer=attrs.offer;
    this.is_acive=attrs.is_acive;
    this.expiredAt=attrs.expiredAt;
    this.deleted=attrs.deleted;
    }

    toJson(){
        return{
            user:this.user,
            offer:this.offer,
            is_acive:this.is_acive,
            expiredAt:this.expiredAt,  
            deleted:this.deleted     
        };
    }


}
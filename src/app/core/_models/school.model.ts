import {ModelInterface} from './model.interface';
import {TrainingOption } from './training-option.model';
 
export class School implements ModelInterface{

    id:number;
    logo:string;
    name:string;
    sigle:string;
  

    constructor(attrs: any = null) {
        if (attrs) {
          this.build(attrs);
        }
      }

    build(attrs: any): void {
      this.id = attrs.id;
      this.logo=attrs.logo;
      this.name=attrs.name;
      this.sigle=attrs.sigle;
    }

    toJson() {
        return {
            id: this.id,
            logo:this.logo,
            name:this.name,
            sigle:this.sigle
          };
    }

}

import { ModelInterface} from "./model.interface";
import { SchoolYear } from './school-year.model';
import { SchoolMatter } from './school-matter.model';


export class TpAndReport implements ModelInterface {

    id:number;
    schoolYear:SchoolYear;
    schoolMatter:SchoolMatter;
    subject:string;
    description:string;
    file:string;
    deleted:boolean;
 

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.schoolYear=attrs.schoolYear;
        this.schoolMatter=attrs.schoolMatter;
        this.subject=attrs.subject;
        this.description=attrs.description;
        this.file=attrs.file;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return{
                id:this.id,
                schoolYear:this.schoolYear,
                schoolMatter:this.schoolMatter,
                subject:this.subject,
                description:this.description,
                file:this.file,
                deleted:this.deleted,                
        };
    }

}

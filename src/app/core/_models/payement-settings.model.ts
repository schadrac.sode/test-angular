import { ModelInterface } from "./model.interface";
 
export class PayementSettings implements ModelInterface {

    id:number;
    name:string;
    numero:number;
    deleted:boolean;
    status:boolean;
    constructor(attrs:any= null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs):void{
        this.id=attrs.id;
        this.name=attrs.name;
        this.numero=attrs.numero;
        this.status=attrs.status;
        this.deleted=attrs.deleted;
    }
    
    toJson() {
        return {
            id:this.id,
            name:this.name,
            numero:this.numero,
            status:this.status,
            deleted:this.deleted
        };
    }
}

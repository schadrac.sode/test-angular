import { ModelInterface } from "./model.interface";
import { Course } from './course.model';

export class CourseFile implements ModelInterface {
    id:number;
    course:Course;
    name:string;
    file:string;
    type:string;
    deleted:boolean;

/*
"id": "string",
"course": 
{},
"name": "string",
"type": "string",
"file": "http://example.com",
"deleted": true,

*/
    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.course=attrs.course;
        this.name=attrs.name;
        this.file=attrs.file;
        this.type=attrs.type;
    }

    toJson(){
        return{
            id:this.id,
            course:this.course,
            name:this.name,
            file:this.file,
            type:this.type,
            deleted:this.deleted
        };
    }
}

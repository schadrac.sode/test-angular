import { ModelInterface } from "./model.interface";

export class Permission implements ModelInterface{
    id:number;
    name: string;

    constructor(attrs: any = null) {
        if (attrs) {
          this.build(attrs);
        }
      }

      build(attrs: any): void {
        this.id=attrs.id;
        this.name = attrs.name;
 
      }
    toJson() {
        return {
            id:this.id,
            name: this.name,  
          };
    }
}

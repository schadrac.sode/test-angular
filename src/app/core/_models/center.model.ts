import { ModelInterface} from "./model.interface";
import { Training } from "./training.model";
export class Center implements  ModelInterface {

    id:number;
    name:string;;//required
    sigle:string;//required
    logo:string;//uri
    satus:boolean;
    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.name=attrs.name;
        this.sigle=attrs.sigle;
        this.logo=attrs.logo;
        this.satus=attrs.satus;
    
    }

    toJson(){
        return{
                id:this.id,
                name:this.name,
                sigle:this.sigle,
                logo:this.logo,
                satus:this.satus
        };
    }

}

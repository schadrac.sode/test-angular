import { School } from './school.model';
import { ModelInterface } from './model.interface';
import { Roles } from './roles';
import { Permission } from './permission.model';
import { Offer } from './offer.model';
export class User implements ModelInterface {
  id: number;
  country: string;
  last_name: string;
  first_name: string;
  username: string;
  phone: string;
  adress: string;
  profil_image: string;
  email: string;
  is_active: number;
  token: string;
  sex: string;
  job: string;
  school: School;
  created_at: string;
  role?: Roles;
  permissions: Permission[];
  offers: Offer[];

  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs: any): void {
    this.id = attrs.id;
    this.is_active = attrs.is_active;
    this.email = attrs.email;
    this.last_name = attrs.last_name;
    this.first_name = attrs.first_name;
    this.username = attrs.username;
    this.phone = attrs.phone;
    this.adress = attrs.adress;
    this.profil_image = attrs.profil_image;
    this.sex = attrs.sex;
    this.country = attrs.country;
    this.job = attrs.job;
    this.school = attrs.school;
    this.token = attrs.token;
    this.created_at = attrs.created_at;
    this.role = attrs.role;
    this.permissions = attrs.permissions;
    this.offers = attrs.offers;
  }
  toJson() {
    return {
      id: this.id,
      is_active: this.is_active,
      email: this.email,
      last_name: this.last_name,
      first_name: this.first_name,
      phone: this.phone,
      adress: this.adress,
      profil_image: this.profil_image,
      sex: this.sex,
      country: this.country,
      job: this.job,
      school: this.school,
      username: this.username,
      token: this.token,
      created_at: this.created_at,
      role: this.role,
      permissions: this.permissions,
      offers: this.offers,
    };
  }
}

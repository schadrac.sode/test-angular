import { ModelInterface } from './model.interface';
import { Center } from './center.model';
import { Training } from './training.model';

export class TrainingCenter implements ModelInterface {
  id: number;
  center: Center; //required
  training: Training; //required
  type: string;
  price: string;
  infos: string;
  deleted: boolean;
  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs: any): void {
    this.id = attrs.id;
    this.center = attrs.center;
    this.type = attrs.type;
    this.price = attrs.price;
    this.infos = attrs.infos;
    this.training = attrs.training;
    this.deleted = attrs.deleted;
  }

  toJson() {
    return {
      id: this.id,
      center: this.center,
      type: this.type,
      price: this.price,
      infos: this.infos,
      training: this.training,
      deleted: this.deleted,
    };
  }
}

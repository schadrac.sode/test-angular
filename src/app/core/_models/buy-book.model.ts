import { ModelInterface } from './model.interface';
import { User } from './user.model';
import { Book } from './book.model';

export class BuyBook implements ModelInterface {
  /*
"id": "string",
"user":
{},
"book":
{},
"price": 0,
"status": true,
"deleted": true,
"createdAt": "2020-09-13T21:30:52Z",
"updatedAt": "2020-09-13T21:30:52Z"
*/
  id: number;
  price: number;
  user: User;
  book: Book;
  refTransaction: string;
  status: boolean;
  deleted: boolean;

  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs): void {
    this.id = attrs.id;
    this.price = attrs.price;
    this.user = attrs.user;
    this.book = attrs.book;
    this.refTransaction = attrs.refTransaction;
    this.status = attrs.status;
    this.deleted = attrs.deleted;
  }

  toJson() {
    return {
      id: this.id,
      price: this.price,
      user: this.user,
      book: this.book,
      refTransaction: this.refTransaction,
      status: this.status,
      deleted: this.deleted,
    };
  }
}

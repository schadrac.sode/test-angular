import { ModelInterface } from "./model.interface";
import { Center } from "./center.model";

export class Training implements ModelInterface {

    id: number;
    name: string;
    status: boolean;
    deleted: boolean;


    constructor(attrs: any = null) {
        if (attrs) {
            this.build(attrs);
        }
    }

    build(attrs: any): void {
        this.id = attrs.id;
        this.name = attrs.name;
        this.status = attrs.status;
        this.deleted = attrs.deleted;
    }

    toJson() {
        return {
            id: this.id,
            name: this.name,
            status: this.status,
            deleted: this.deleted,
        };
    }
}

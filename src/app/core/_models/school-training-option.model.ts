import { ModelInterface } from "./model.interface";
import { TrainingOption } from './training-option.model';
import { School } from "./school.model";

export class SchoolTrainingOption implements ModelInterface {
    
    id:number;
    school:School;
    trainingOption:TrainingOption;
    deleted:boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.trainingOption=attrs.trainingOption;
        this.school=attrs.school;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return{
            id:this.id,
            trainingOption:this.trainingOption,
            school:this.school,
            deleted:this.deleted
        };
    }
}

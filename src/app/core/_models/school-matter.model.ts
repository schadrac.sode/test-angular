import { ModelInterface } from "./model.interface";
import { SchoolTrainingOption } from './school-training-option.model';
import { Matter } from './matter.model';

export class SchoolMatter implements ModelInterface {
    
    id: number;
    matter: Matter;
    schoolTrainingOption: SchoolTrainingOption;
    deleted: boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.matter=attrs.matter;
        this.schoolTrainingOption=attrs.schoolTrainingOption;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return{
            id:this.id,
            matter:this.matter,
            schoolTrainingOption:this.schoolTrainingOption,
            deleted:this.deleted
        };
    }
}

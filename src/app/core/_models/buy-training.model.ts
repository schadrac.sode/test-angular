import { ModelInterface } from "./model.interface";
import { User } from './user.model';
import { TrainingCenter } from './training-center.model';


export class BuyTraining implements ModelInterface {
/*
*/
    id:number;
    price:number;
    user:User;
    trainingCenter:TrainingCenter;
    status:boolean;
    deleted:boolean;
    constructor(attrs:any= null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs):void{
        this.id=attrs.id;
        this.price=attrs.price;
        this.user=attrs.user;
        this.trainingCenter=attrs.trainingCenter;
        this.status=attrs.status;
        this.deleted=attrs.deleted;
    }

    toJson() {
        return {
            id:this.id,
            price:this.price,
            user:this.user,
            trainingCenter:this.trainingCenter,
            status:this.status,
            deleted:this.deleted
        };
    }
}

import { ModelInterface } from "./model.interface";
import { Matter } from "./matter.model";

export class TrainingOption implements ModelInterface {

    id:number;
    name:string;
    deleted:boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void{
        this.id=attrs.id;
        this.name=attrs.name;
        this.deleted=attrs.deleted;
    }
    toJson(){
        return {
            id:this.id,
            name:this.name,
            deleted:this.deleted
        };
    }

}

import { ModelInterface } from "./model.interface";
import { User } from "./user.model";
import { Permission } from "./permission.model";

export class Delegate implements ModelInterface{
    

    id:number;
    user:User;
    permission: Permission;
    deleted:boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.user=attrs.user;
        this.permission=attrs.permission;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return{
            id:this.id,
            user:this.user,
            permission:this.permission,
            deleted:this.deleted,
        };
    }
}

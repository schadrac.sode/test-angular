import { ModelInterface } from "./model.interface";
import { Course } from "./course.model";
export class CatCourse implements ModelInterface {

    id:number;
    name:string;
    courses:Course[];

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
            this.id=attrs.id;
            this.name=attrs.name;
            this.courses=attrs.courses;
    }

    toJson(){
        return{
            id :this.id,
            name : this.name ,
            courses:this.courses
        };
    }

}

import { ModelInterface } from "./model.interface";
import { Test } from "./test.model";

export class CatTest implements  ModelInterface {

    id:number;
    name:string;
    tests:Test[];
    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.name=attrs.name;
        this.tests=attrs.tests;

    }

    toJson(){
        return{
            id:this.id,
            name:this.name,
            tests:this.tests
        };
    }


}

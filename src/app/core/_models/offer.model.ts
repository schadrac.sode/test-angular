import { ModelInterface } from './model.interface';

export class Offer implements ModelInterface {
  id: number;
  name: string;
  description: string;
  price: number;
  duration: number;
  status: boolean;
  module: any[];

  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs: any): void {
    this.id = attrs.id;
    this.name = attrs.name;
    this.description = attrs.description;
    this.price = attrs.price;
    this.duration = attrs.duration;
    this.status = attrs.status;
    this.module = attrs.module;
  }

  toJson() {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      price: this.price,
      duration: this.duration,
      status: this.status,
      module: this.module,
    };
  }
}

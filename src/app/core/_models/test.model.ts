import { ModelInterface } from './model.interface';
import { SchoolMatter } from './school-matter.model';
import { SchoolYear } from './school-year.model';

export class Test implements ModelInterface {
  id: number;
  schoolMatter: SchoolMatter;
  schoolYear: SchoolYear;
  name: string;
  type: string;
  file: string;
  duration: number;
  level: number;
  period: string;
  numPeriod: number;
  deleted: boolean;
  correction: string;
  /*
    "schoolMatter": "string",
    "schoolYear": "string",
    "name": "string",
    "type": "string",
    "duration": 0,
    "period": "string",
    "numPeriod": 0,
    "deleted": true
*/

  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs: any): void {
    this.id = attrs.id;
    this.schoolMatter = attrs.schoolMatter;
    this.name = attrs.name;
    this.type = attrs.type;
    this.duration = attrs.duration;
    this.level = attrs.level;
    this.period = attrs.period;
    this.numPeriod = attrs.numPeriod;
    this.deleted = attrs.deleted;
  }

  /*
    "schoolMatter": "string",
    "schoolYear": "string",
    "name": "string",
    "type": "string",
    "duration": 0,
    "period": "string",
    "numPeriod": 0,
    "deleted": true
*/

  toJson() {
    return {
      id: this.id,
      schoolMatter: this.schoolMatter,
      schoolYear: this.schoolYear,
      name: this.name,
      type: this.type,
      duration: this.duration,
      level: this.level,
      period: this.period,
      numPeriod: this.numPeriod,
      deleted: this.deleted,
    };
  }
}

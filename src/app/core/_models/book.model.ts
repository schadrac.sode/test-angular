import { ModelInterface } from './model.interface';
import { School } from './school.model';

export class Book implements ModelInterface {
  /*

*/
  id: number;
  year: string;
  page: number;
  name: string;
  school: string;
  price: number;
  type: string;
  author: string;
  edition: string;
  file: string;
  cover: string;
  deleted: boolean;
  constructor(attrs: any = null) {
    if (attrs) {
      this.build(attrs);
    }
  }

  build(attrs): void {
    this.id = attrs.id;
    this.year = attrs.year;
    this.page = attrs.page;
    this.name = attrs.name;
    this.school = attrs.school;
    this.type = attrs.type;
    this.price = attrs.price;
    this.author = attrs.author;
    this.edition = attrs.edition;
    this.file = attrs.file;
    (this.cover = attrs.cover), (this.deleted = attrs.deleted);
  }

  toJson() {
    return {
      id: this.id,
      year: this.year,
      page: this.page,
      name: this.name,
      school: this.school,
      type: this.type,
      price: this.price,
      author: this.author,
      edition: this.edition,
      file: this.file,
      cover: this.cover,
      deleted: this.deleted,
    };
  }
}

import { ModelInterface } from "./model.interface";
import { CourseFile } from "./course-file.model";
import { TrainingCenter } from './training-center.model';

export class Course implements ModelInterface{

    id:number;
    trainingCenter:TrainingCenter;//required
    name:string;//required
    author:string;//required
    description:string;//required
    duration:string;//required
    status:boolean;


      constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }

    build(attrs:any):void {
        this.id=attrs.id;
        this.name=attrs.name;
        this.description=attrs.description;
        this.duration=attrs.duration;
        this.status=attrs.status;

    }

    toJson(){
        return{
            id:this.id,
            name:this.name,
            description:this.description,
            duration:this.duration,
            status:this.status
        };
    }

}

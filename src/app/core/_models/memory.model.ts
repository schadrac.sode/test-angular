import { ModelInterface } from "./model.interface";
import { Domain } from 'domain';
import { School } from './school.model';
import { SchoolYear } from './school-year.model';


export class Memory implements ModelInterface {

    id:number;
    domain:Domain;
    school:School;
    schoolYear:SchoolYear;
    subject:string;
    author:string;
    file:string;
    deleted:boolean;

    constructor(attrs:any=null){
        if (attrs) {
            this.build(attrs);
        }
    }
    build(attrs):void {
        this.id=attrs.id;
        this.domain=attrs.domain;
        this.school=attrs.school;
        this.schoolYear=attrs.schoolYear;
        this.subject=attrs.subject;
        this.author=attrs.author;
        this.file=attrs.file;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return {
            id:this.id,
            domain:this.domain,
            school:this.school,
            schoolYear:this.schoolYear,
            subject:this.subject,
            author:this.author,
            file:this.file,
            deleted:this.deleted
        };
    }

}

import { ModelInterface } from "./model.interface";
import * as moment from "moment";



export class SchoolYear implements ModelInterface{

    id:number;
    start:string;
    end:string;
    academie: string;
    deleted:boolean;

    constructor(attrs:any=null){
        if(attrs){
            this.build(attrs);
        }
    }
    build(attrs:any):void{
        this.id=attrs.id;
        this.start=attrs.start;
        this.end=attrs.end;
        this.academie= attrs.start+'-'+attrs.end;
        this.deleted=attrs.deleted;
    }

    toJson(){
        return {
            id:this.id,
            start:this.start,
            end:this.end,
            academie: this.academie,
            deleted:this.deleted
        };
    }


}

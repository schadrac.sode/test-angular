import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { catchError, map } from 'rxjs/operators';
import { Config } from '../../app.config';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  url = Config.toApiUrl("auth");

  constructor(private http: HttpClient) {
  }

  isLoggedIn() {
    return true;
  }
  // Authentication/Authorization
  login(email: string, password: string) {
    var formData = new FormData()
    formData.append("email", email)
    formData.append("password", password)
    return this.http.post(Config.toApiUrl('account/login/'), { email:email, password:password },  { headers: new HttpHeaders({})});
  }

  getUserByToken(): Observable<any> {
    return this.http.get<any>("current-user",  { headers: new HttpHeaders({})});
  }

  register(user: User): Observable<any> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    return this.http.post<User>("", user, { headers: httpHeaders })
      .pipe(
        map((res: User) => {
          return res;
        }),
        catchError(err => {
          return null;
        })
      );
  }

  /*
   * Submit forgot password request
   *
   * @param {string} email
   * @returns {Observable<any>}
   */
  public forgotPassword(email: string): Observable<any> {
    return this.http.post(Config.toApiUrl('password_reset/'), { email: email })
      .pipe(catchError(this.handleError('forgot-password', []))
      );
  }
  public resetPassword(password: string, token: string): Observable<any> {
    return this.http.post('', { password: password, token: token })
      .pipe(catchError(this.handleError('forgot-password', []))
      );
  }

  /*
  * Handle Http operation that failed.
  * Let the app continue.
   *
 * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }

}

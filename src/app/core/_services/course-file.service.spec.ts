import { TestBed } from '@angular/core/testing';

import { CourseFileService } from './course-file.service';

describe('CourseFileService', () => {
  let service: CourseFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourseFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

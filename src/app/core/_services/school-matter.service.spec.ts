import { TestBed } from '@angular/core/testing';

import { SchoolMatterService } from './school-matter.service';

describe('SchoolMatterService', () => {
  let service: SchoolMatterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolMatterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

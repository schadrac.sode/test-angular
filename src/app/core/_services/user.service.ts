import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../app.config';
import { catchError, tap, map } from 'rxjs/operators';
import { Roles } from '../_models/roles';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  url = Config.toApiUrl('account/users/');

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<any[]>(this.url, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }

  get(id) {
    return this.http.get<any[]>(this.url + `${id}/`, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }

  create(ressource) {
    return this.http
      .post<any>(this.url, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`added ressource ${ressource}`))
      );
  }
  update(ressource, id) {
    return this.http
      .put<any>(this.url + `${id}/`, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`updated ressource ${ressource}`))
      );
  }
  update_partial(ressource, id) {
    return this.http
      .patch<any>(this.url + `${id}/`, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`updated ressource ${ressource}`))
      );
  }

  delete(id: number) {
    return this.http.delete<any[]>(this.url + `${id}/realDelete`, {
      headers: Config.httpHeader(),
    });
  }

  lock(id: number, ressource) {
    return this.http.put<any[]>(`${this.url}${id}/desactive/`, ressource, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }
  unlock(id: number, ressource) {
    return this.http.put<any[]>(`${this.url}${id}/active/`, ressource, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }
  set_password(ressource, id) {
    return this.http
      .patch<any>(`${this.url}${id}/set_password/`, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`updated ressource ${ressource}`))
      );
  }

  change_password(value) {
    return this.http.put<any[]>(
      `${this.url}/change/password`,
      value,
      Config.httpHeader()
    );
  }

  getUserEtablishment(id) {
    return this.http.get<any[]>(`${this.url}${id}/etablishment`, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }
}

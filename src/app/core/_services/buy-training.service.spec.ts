import { TestBed } from '@angular/core/testing';

import { BuyTrainingService } from './buy-training.service';

describe('BookService', () => {
  let service: BuyTrainingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuyTrainingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

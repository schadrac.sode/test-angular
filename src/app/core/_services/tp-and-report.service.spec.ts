import { TestBed } from '@angular/core/testing';

import { TpAndReportService } from './tp-and-report.service';

describe('TpAndReportService', () => {
  let service: TpAndReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TpAndReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

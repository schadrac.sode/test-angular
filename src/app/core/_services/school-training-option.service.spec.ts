import { TestBed } from '@angular/core/testing';

import { SchoolTrainingOptionService } from './school-training-option.service';

describe('SchoolTrainingOptionService', () => {
  let service: SchoolTrainingOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolTrainingOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

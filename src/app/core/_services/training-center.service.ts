import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../app.config';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrainingCenterService {
  url = Config.toApiUrl('trainingCenter/');

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<any[]>(this.url, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }

  get(id) {
    return this.http.get<any[]>(this.url + `${id}/`, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }

  create(ressource) {
    return this.http
      .post<any>(this.url, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`added ressource ${ressource}`))
      );
  }
  update(ressource, id) {
    return this.http
      .put<any>(this.url + `${id}/`, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`updated ressource ${ressource}`))
      );
  }
  update_partial(ressource, id) {
    return this.http
      .patch<any>(this.url + `${id}/`, ressource, {
        headers: new HttpHeaders({
          Authorization: 'Token ' + localStorage.getItem('seweToken'),
        }),
      })
      .pipe(
        tap((ressource: any) => console.log(`updated ressource ${ressource}`))
      );
  }

  delete(id: number) {
    return this.http.delete<any[]>(this.url + `${id}/realDelete`, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }

  delete_partial(id: number) {
    return this.http.delete<any[]>(this.url + `${id}`, {
      headers: new HttpHeaders({
        Authorization: 'Token ' + localStorage.getItem('seweToken'),
      }),
    });
  }
}

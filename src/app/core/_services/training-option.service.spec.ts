import { TestBed } from '@angular/core/testing';

import { TrainingOptionService } from './training-option.service';

describe('TrainingOptionService', () => {
  let service: TrainingOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrainingOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

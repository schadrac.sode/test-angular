import { TestBed } from '@angular/core/testing';

import { PayementSettingsService } from './payement-settings.service';

describe('PayementSettingsService', () => {
  let service: PayementSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PayementSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

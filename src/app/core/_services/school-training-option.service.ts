import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Config } from '../../app.config';
import { catchError, tap, map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class SchoolTrainingOptionService {


 url= Config.toApiUrl('schoolTrainingOption/');
  constructor(private http:HttpClient) { }


  getAll(){

    return this.http.get<any[]>(this.url, Config.httpHeader(localStorage.getItem("seweToken"),true));
  }

  get(id){

    return this.http.get<any[]>(this.url+`${id}/`, Config.httpHeader(localStorage.getItem("seweToken"),true));
  }
  create(ressource){
    return this.http.post<any>(this.url, ressource,
    Config.httpHeader(localStorage.getItem("seweToken"),true)).pipe(
      tap((ressource: any) => console.log(`added ressource ${ressource}`))
    );
  }
  update(ressource,id){
    return this.http.put<any>(this.url+`${id}/`, ressource,  Config.httpHeader(localStorage.getItem("seweToken"),true)).pipe(
      tap((ressource: any) => console.log(`updated ressource ${ressource}`))
    );
  }
  update_partial(ressource,id){
    return this.http.patch<any>(this.url+`${id}/`, ressource,  Config.httpHeader(localStorage.getItem("seweToken"),true)).pipe(
      tap((ressource: any) => console.log(`updated ressource ${ressource}`))
    );
  }

  delete(id:number){
    return this.http.delete<any[]>(this.url+`${id}/realDelete/`,Config.httpHeader());
  }
  delete_partial(id:number){
    return this.http.delete<any[]>(this.url+`${id}/`,Config.httpHeader());
  }

}

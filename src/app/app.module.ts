import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CountUpModule } from 'ngx-countup';
//partials
import { AsideComponent } from './views/partials/aside/aside.component';
import { HeaderComponent } from './views/partials/header/header.component';
import { FooterComponent } from './views/partials/footer/footer.component';
import { HeaderMobileComponent } from './views/partials/header-mobile/header-mobile.component';
import { ScrollToTopComponent } from './views/partials/scroll-to-top/scroll-to-top.component';
import { NotifPanelComponent } from './views/partials/notif-panel/notif-panel.component';
import { UserPanelComponent } from './views/partials/user-panel/user-panel.component';

//pages
import { ProfilComponent } from './views/pages/profil/profil.component';
import { BaseComponent } from './views/pages/base/base.component';
import { DashboardComponent } from './views/pages/dashboard/dashboard.component';
import { Error403Component } from './views/pages/error403/error403.component';
import { LoginComponent } from './views/pages/auth/login/login.component';
import { ForgotPasswordComponent } from './views/pages/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './views/pages/auth/reset-password/reset-password.component';
import { CrudComponent } from './views/pages/crud/crud.component';

import { HighlightModule } from 'ngx-highlightjs';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { WebsocketService } from './core/_services/websocket.service';
import { ChartsModule } from 'ng2-charts';

//crud
import { SchoolComponent } from './views/pages/crud/school/school.component';
import { SchoolTrainingOptionComponent } from './views/pages/crud/school-training-option/school-training-option.component';
import { MatterComponent } from './views/pages/crud/matter/matter.component';
import { SchoolYearComponent } from './views/pages/crud/school-year/school-year.component';
import { DomainComponent } from './views/pages/crud/domain/domain.component';
import { OfferComponent } from './views/pages/crud/offer/offer.component';
import { CenterComponent } from './views/pages/crud/center/center.component';
import { TrainingComponent } from './views/pages/crud/training/training.component';
import { CourseComponent } from './views/pages/crud/course/course.component';
import { CourseFileComponent } from './views/pages/crud/course-file/course-file.component';
import { DelegateComponent } from './views/pages/crud/delegate/delegate.component';
import { BookComponent } from './views/pages/crud/book/book.component';
import { MemoryComponent } from './views/pages/crud/memory/memory.component';
import { PermissionComponent } from './views/pages/crud/permission/permission.component';
import { SchoolMatterComponent } from './views/pages/crud/school-matter/school-matter.component';
import { SubscriptionComponent } from './views/pages/crud/subscription/subscription.component';
import { TestComponent } from './views/pages/crud/test/test.component';
import { TpAndReportComponent } from './views/pages/crud/tp-and-report/tp-and-report.component';
import { TrainingCenterComponent } from './views/pages/crud/training-center/training-center.component';
import { TrainingOptionComponent } from './views/pages/crud/training-option/training-option.component';
import { UserComponent } from './views/pages/crud/user/user.component';
import { BuyTrainingComponent } from './views/pages/crud/buy-training/buy-training.component';
import { BuyBookComponent } from './views/pages/crud/buy-book/buy-book.component';
import { PayementSettingsComponent } from './views/pages/crud/payement-settings/payement-settings.component';

import { LocalizetPipe } from './localizet.pipe';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { TruncateTextPipe } from './core/_pipes/truncate-text.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AsideComponent,
    LoginComponent,
    ForgotPasswordComponent,
    BaseComponent,
    DashboardComponent,
    Error403Component,
    HeaderComponent,
    FooterComponent,
    HeaderMobileComponent,
    ScrollToTopComponent,
    NotifPanelComponent,
    UserPanelComponent,
    ProfilComponent,
    CrudComponent,
    ResetPasswordComponent,
    SchoolComponent,
    SchoolTrainingOptionComponent,
    MatterComponent,
    SchoolYearComponent,
    DomainComponent,
    OfferComponent,
    CenterComponent,
    TrainingComponent,
    CourseComponent,
    CourseFileComponent,
    DelegateComponent,
    BookComponent,
    MemoryComponent,
    PermissionComponent,
    SchoolMatterComponent,
    SubscriptionComponent,
    TestComponent,
    TpAndReportComponent,
    TrainingCenterComponent,
    TrainingOptionComponent,
    UserComponent,
    BuyBookComponent,
    BuyTrainingComponent,
    PayementSettingsComponent,
    LocalizetPipe,
    TruncateTextPipe,
  ],
  imports: [
    NgSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HighlightModule,
    ChartsModule,
    CountUpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    DecimalPipe,
    { provide: LOCALE_ID, useValue: 'en-US' },
    WebsocketService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

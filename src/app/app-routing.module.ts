import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilComponent } from './views/pages/profil/profil.component';
import { BaseComponent } from './views/pages/base/base.component';
import { DashboardComponent } from './views/pages/dashboard/dashboard.component';
import { Error403Component } from './views/pages/error403/error403.component';
import { LoginComponent } from './views/pages/auth/login/login.component';
import { ForgotPasswordComponent } from './views/pages/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './views/pages/auth/reset-password/reset-password.component';
import { AuthRoleGuard } from './core/_guards/auth-role.guard';
import { IsAuthGuard } from './core/_guards/is-auth.guard';


import { CrudComponent } from './views/pages/crud/crud.component';
import { SchoolComponent } from './views/pages/crud/school/school.component';
import { SchoolTrainingOptionComponent } from './views/pages/crud/school-training-option/school-training-option.component';
import { MatterComponent } from './views/pages/crud/matter/matter.component';
import { SchoolYearComponent } from './views/pages/crud/school-year/school-year.component';
import { DomainComponent } from './views/pages/crud/domain/domain.component';
import { OfferComponent } from './views/pages/crud/offer/offer.component';
import { CenterComponent } from './views/pages/crud/center/center.component';
import { TrainingComponent } from './views/pages/crud/training/training.component';
import { CourseComponent } from './views/pages/crud/course/course.component';
import { CourseFileComponent } from './views/pages/crud/course-file/course-file.component';
import { DelegateComponent } from './views/pages/crud/delegate/delegate.component';
import { BookComponent } from './views/pages/crud/book/book.component';
import { MemoryComponent } from './views/pages/crud/memory/memory.component';
import { PermissionComponent } from './views/pages/crud/permission/permission.component';
import { SchoolMatterComponent } from './views/pages/crud/school-matter/school-matter.component';
import { SubscriptionComponent } from './views/pages/crud/subscription/subscription.component';
import { TestComponent } from './views/pages/crud/test/test.component';
import { TpAndReportComponent } from './views/pages/crud/tp-and-report/tp-and-report.component';
import { TrainingCenterComponent } from './views/pages/crud/training-center/training-center.component';
import { TrainingOptionComponent } from './views/pages/crud/training-option/training-option.component';
import { UserComponent } from './views/pages/crud/user/user.component';
import { BuyTrainingComponent } from './views/pages/crud/buy-training/buy-training.component';
import { BuyBookComponent } from './views/pages/crud/buy-book/buy-book.component';
import { PayementSettingsComponent } from './views/pages/crud/payement-settings/payement-settings.component';


const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
    canActivate: [IsAuthGuard],
  },
  {
    path: "reset-password/:token",
    component: ResetPasswordComponent
  },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent
  },
  {
    path: "login/:lang",
    component: LoginComponent
  },
  {
    path: "reset-password/:lang/:token",
    component: ResetPasswordComponent
  },
  {
    path: "forgot-password/:lang",
    component: ForgotPasswordComponent
  },
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthRoleGuard],
    children: [

      {
        path: 'crud',
        component: CrudComponent,
      },
      {
        path: 'school',
        component: SchoolComponent
      },
      {
        path: 'course/:training_center',// in p
        component: CourseComponent
      },
      {
        path: 'course-file/:course', //in p
        component: CourseFileComponent
      },
      {
        path: 'delegate',// in p
        component: DelegateComponent
      },
      {
        path: 'livre',
        component: BookComponent
      },
      {
        path: 'memory',
        component: MemoryComponent
      },
      {
        path: 'permission', // in p
        component: PermissionComponent
      },
      {
        path: 'school-matter/:school_training_option',
        component: SchoolMatterComponent
      },
      {
        path: 'subscription', // in p
        component: SubscriptionComponent
      },
      {
        path: 'test',
        component: TestComponent
      },
      {
        path: 'tp-and-report', // in p
        component: TpAndReportComponent
      },
      {
        path: 'training-center',// in p
        component: TrainingCenterComponent
      },
      {
        path: 'training-option',
        component: TrainingOptionComponent
      },
      {
        path: 'center',
        component: CenterComponent
        // canActivate: [IsAuthGuard]
      },
      {
        path: 'school',
        component: SchoolComponent
        // canActivate: [IsAuthGuard]
      },
      {
        path: 'matter',
        component: MatterComponent
      },
      {
        path: 'domain',
        component: DomainComponent
      },
      {
        path: 'school-year',
        component: SchoolYearComponent
      },
      {
        path: 'school-training-option/:school',
        component: SchoolTrainingOptionComponent
      },
      {
        path: 'training',
        component: TrainingComponent
      },
      {
        path: 'offer',
        component: OfferComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'profil',
        component: ProfilComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'buy-book',
        component: BuyBookComponent
      },
      {
        path: 'buy-training',
        component: BuyTrainingComponent
      },
      {
        path: 'payement-settings',
        component: PayementSettingsComponent
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
    ],
  },
  { path: '**', redirectTo: 'error/403', pathMatch: 'full' },
  {
    path: 'error/403',
    component: Error403Component

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

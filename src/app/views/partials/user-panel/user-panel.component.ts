import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalService } from 'src/app/core/_services/browser-storages/local.service';
import { Roles } from 'src/app/core/_models/roles';
import {TranslateService} from '@ngx-translate/core';
import { UserService } from 'src/app/core/_services/user.service';


@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {

  constructor(private userService:UserService,private router:Router,private localStorageService:LocalService,private translateService: TranslateService) { }

  current_role=""
  user:any

  ngOnInit(): void {
    this.current_role=localStorage.getItem('seweUserRole')
      this.user=this.localStorageService.getJsonValue("seweUserData")
       if(this.current_role!=Roles.Admin && this.current_role!=Roles.SubAdmin){
       }


      //this.translateService.use(this.user.default_language);
  }




  signout(){
    localStorage.removeItem('seweToken');
    // localStorage.removeItem('seweUserRole')
    this.localStorageService.clearToken();
    this.router.navigateByUrl('/login');
  }

}

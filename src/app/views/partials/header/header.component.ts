import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalService } from 'src/app/core/_services/browser-storages/local.service';
import { Roles } from 'src/app/core/_models/roles';
import { UserService } from 'src/app/core/_services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private localStorageService: LocalService, private userService: UserService) { }
  current_role = ""
  user: any

  Fr = "fr"
  En = "en"
  default_lang = ""
  ngOnInit(): void {
    // this.current_role = localStorage.getItem('seweUserRole')
    if(localStorage.getItem("seweUserData")!=null){
     // this.user = this.localStorageService.getJsonValue("seweUserData")
    }
    //console.log(this.user)
    if (this.current_role != Roles.Admin && this.current_role != Roles.SubAdmin) {
    }

  }

  useLanguage(lang: string) {
    this.userService.update_partial({ default_language: lang }, this.user.id).subscribe((res) => {
      this.localStorageService.setJsonValue("seweUserData", res);
      this.ngOnInit()
      window.location.reload();
    })

  }
}

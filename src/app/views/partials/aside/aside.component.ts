import { Component, OnInit } from '@angular/core';
import { Roles } from 'src/app/core/_models/roles';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  admin_r=Roles.Admin
  sub_admin_r=Roles.SubAdmin
  super_admin_r=Roles.SuperAdmin
  user_r=Roles.User

  current_user_role=''
  constructor(private router:Router) { }
  // agence_location_r=Roles.AgenceLocationVoiture
  // restaurant_r=Roles.Restaurant
  // hotel_r=Roles.Hotel
  // tour_operator_r=Roles.TourOperateur

  // SuperAdmin = 'super-admin',
  // Admin = 'admin',
  // SubAdmin= 'sous-admin',
  // USer= 'user',

  ngOnInit(): void {
    this.current_user_role=localStorage.getItem('seweUserRole')
  }


}

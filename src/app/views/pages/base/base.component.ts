import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/_services/user.service';
import { LocalService } from 'src/app/core/_services/browser-storages/local.service';
import {TranslateService} from '@ngx-translate/core';
import { Roles } from 'src/app/core/_models/roles';


@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  constructor(private userService:UserService,private localService:LocalService,private translateService: TranslateService) { }

  ngOnInit(): void {
  //   let user=this.localService.getJsonValue('seweUserData')
  //   if(localStorage.getItem('seweUserRole')!=Roles.Admin){
     
  //   }
  //   this.translateService.use(user.default_language);
   }

}

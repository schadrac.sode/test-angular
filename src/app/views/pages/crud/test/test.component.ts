import { Matter } from 'src/app/core/_models/matter.model';
import { MatterService } from 'src/app/core/_services/matter.service';
import { TrainingOptionService } from 'src/app/core/_services/training-option.service';
import { SchoolService } from './../../../../core/_services/school.service';
import { SchoolTrainingOptionService } from './../../../../core/_services/school-training-option.service';
import { SchoolYear } from './../../../../core/_models/school-year.model';
import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { TestService } from 'src/app/core/_services/test.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Test } from 'src/app/core/_models/test.model';
import { SchoolYearService } from 'src/app/core/_services/school-year.service';
import { SchoolMatterService } from 'src/app/core/_services/school-matter.service';
import { SchoolTrainingOption } from 'src/app/core/_models/school-training-option.model';
import { TrainingOption } from 'src/app/core/_models/training-option.model';
import { School } from 'src/app/core/_models/school.model';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
})
export class TestComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';

  uploadResponse = { status: '', message: '', filePath: '' };
  uploadCResponse = { status: '', message: '', filePath: '' };
  progress: number = 0;
  progressC: number = 0;
  err = {
    message: 'Erreur',
  };

  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Test();
  file: File;
  urls;
  fichier: File;
  lien;

  selectedSchoolYear: any;
  selectedSchoolMatter: any;
  /**/
  selectedSchoolMatterItems: any;
  selectedSchoolMatterItem: any;

  testSchoolMatterItems = [];

  selectedSchoolItems: any;
  testSchoolItems = [];

  selectedSchoolTrainingOptionItems: any;
  testSchoolTrainingOptionItems: SchoolTrainingOption[] = [];

  selectedSchoolYearItem: any;

  selectedSchoolYearItems: any;
  testSchoolYearItems: SchoolYear[] = [];

  selectedtestPeriodIItems: any;
  testPeriodItems = [
    {
      id: 'trimestre',
      name: 'Trimestre',
    },
    {
      id: 'semestre',
      name: 'Semestre',
    },
  ];

  selectedtestLevelItems: any;
  testLevelItems = [
    {
      id: 1,
      name: '1ère Année',
    },
    {
      id: 2,
      name: '2ème Année',
    },
    {
      id: 3,
      name: '3ème Année',
    },
    {
      id: 4,
      name: '4ème Année',
    },
    {
      id: 5,
      name: '5ème Année',
    },
  ];

  selectedtestTypeItems: any;
  testTypeItems = [
    {
      id: 'devoir',
      name: 'Devoir',
    },
    {
      id: 'partiel',
      name: 'Partiel',
    },
  ];

  selectedtestPeriodTriItems: any;
  testPeriodTriItems = [
    {
      id: '1',
      name: 'Premier trimestre',
    },
    {
      id: '2',
      name: 'Deuxème trimestre',
    },
    {
      id: '3',
      name: 'Troisième trimestre',
    },
  ];

  selectedtestPeriodSemItems: any;
  testPeriodSemItems = [
    {
      id: '1',
      name: 'Premier semestre',
    },
    {
      id: '2',
      name: 'Second semestre',
    },
  ];

  changes;
  schoolSelected = false;
  selectedTrainingOption: any;
  selectedSchool: any;
  selectedMatter: any;
  trainingOptionItems: TrainingOption[] = [];
  schoolItems: School[] = [];
  matterItems: Matter[] = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  filterBySchool(event) {
    console.log(event);
    if (event !== null) {
      this.testService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.schoolMatter.schoolTrainingOption.school.id === event) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.testService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  filterByTrainingOption(event) {
    console.log(event);
    if (event !== null) {
      this.testService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (
            val.schoolMatter.schoolTrainingOption.trainingOption.id === event
          ) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.testService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  filterByMatter(event) {
    console.log(event);
    if (event !== null) {
      this.testService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.schoolMatter.matter.id === event) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.testService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }
  filterByLevel(event) {
    console.log(event);
    if (event) {
      this.testService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.level === event.id) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.testService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    console.log(el);
    this.selectedSchoolItems = el.schoolMatter.schoolTrainingOption.school.id;
    this.selectedSchoolTrainingOptionItems =
      el.schoolMatter.schoolTrainingOption.id;
    this.selectedSchoolMatterItems = el.schoolMatter.id;
    this.selectedtestLevelItems = el.level;
    this.selectedtestTypeItems = el.type;
    this.selectedtestPeriodTriItems = el.numPeriod;
    this.selectedtestPeriodIItems = el.period;
    this.selectedSchoolYearItems = el.schoolYear.id;
    this.selected_data = el;

    // this.selected_data.id=el.id;
    // this.selected_data.name=el.name;
    // this.selected_data.type=el.type;
    // this.selected_data.file=el.file;
    // this.selected_data.duration=el.duration;
    // this.selected_data.period=el.period;
    // this.selected_data.numPeriod=el.numPeriod;
    // this.selected_data.schoolMatter=el.schoolMatter;
    // this.selected_data.schoolYear=el.schoolYear;

    console.log(this.selected_data);

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private testService: TestService,
    private schoolYearService: SchoolYearService,
    private schoolMatterService: SchoolMatterService,
    private matterService: MatterService,
    private schoolService: SchoolService,
    private trainingOptionService: TrainingOptionService,
    private schoolTrainingOptionService: SchoolTrainingOptionService
  ) {
    this.schoolService.getAll().subscribe((res) => {
      this.schoolItems = res;
    });
    this.trainingOptionService.getAll().subscribe((res) => {
      this.trainingOptionItems = res;
    });
    this.matterService.getAll().subscribe((res) => {
      this.matterItems = res;
    });
    this.schoolYearService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.testSchoolYearItems;

      res.forEach((el: SchoolYear) => {
        el.academie = `${el.start}-${el.end}`;
        this.testSchoolYearItems.push(el);
        console.log('Anne academique ' + this.testSchoolYearItems);
        console.log(this.testSchoolYearItems);
      });
    });

    this.schoolService.getAll().subscribe((res) => {
      this.testSchoolItems = res;
      console.log('Ecole ' + this.testSchoolItems);
      console.log(this.testSchoolItems);
    });

    this.schoolMatterService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.testSchoolMatterItems = res;
      console.log('Matieres ' + this.testSchoolMatterItems);
      console.log(this.testSchoolMatterItems);
    });

    this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.testSchoolTrainingOptionItems = res;
      // console.log('Option d formation ');
      // console.log('Option d formation ' + this.testSchoolTrainingOptionItems);
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.testService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  change(event) {
    console.log(event);
    if (event && event.id === 'trimestre') {
      this.changes = event.id;
    }
    if (event && event.id === 'semestre') {
      this.changes = event.id;
    }

    if (!event) {
      this.changes = event;
    }
  }

  onScoolSelected(event) {
    this.schoolSelected = true;
    console.log(event);
    const id = event.id;

    this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
      console.log(res);
      // let t = this;
      this.testSchoolTrainingOptionItems = [];
      res.forEach((el) => {
        if (id === el.school.id) {
          this.testSchoolTrainingOptionItems.push(el);
        }
        console.log(this.testSchoolTrainingOptionItems);
      });
    });
  }

  onFiliereSelected(event) {
    this.schoolSelected = true;
    console.log(event);
    const id = event.id;

    this.schoolMatterService.getAll().subscribe((res: any) => {
      console.log(res);
      // let t = this;
      this.testSchoolMatterItems = [];
      res.forEach((el) => {
        if (id === el.schoolTrainingOption.id) {
          this.testSchoolMatterItems.push(el);
        }
        console.log(this.testSchoolMatterItems);
      });
    });
  }

  create(value) {
    console.log(value);
    const form = new FormData();
    if (this.fichier && this.fichier.name) {
      form.append('file', this.file, this.file.name);
    }

    if (this.fichier && this.fichier.name) {
      form.append('correction', this.fichier, this.fichier.name);
    }
    form.append('name', value.testName);
    form.append('duration', value.testDuration);
    form.append('type', this.selectedtestTypeItems);
    form.append('level', this.selectedtestLevelItems);
    form.append('period', this.selectedtestPeriodIItems);
    // form.append('schoolMatter', this.selectedSchoolMatterItems);
    // form.append('schoolYear', this.selectedSchoolYearItems);
    // console.log('Items' + this.selectedSchoolItems);
    // console.log('Matters' + this.selectedSchoolMatterItems);
    let schhoolMatterId = this.selectedSchoolMatterItems;
    // console.log(schhoolMatterId);
    if (this.changes === 'trimestre') {
      form.append('numPeriod', this.selectedtestPeriodTriItems);
    } else if (this.changes === 'semestre') {
      form.append('numPeriod', this.selectedtestPeriodSemItems);
    }
    // console.log(value.testSchoolMatter);

    this.schoolYearService
      .get(this.selectedSchoolYearItems)
      .subscribe((res) => {
        console.log(res);
        this.selectedSchoolYearItem = res;
        console.log(this.selectedSchoolYearItem);
        form.append('schoolYear', this.selectedSchoolYearItem.id);

        //this.selectedSchoolYearItems
        console.log(schhoolMatterId);
        console.log(value.testSchoolMatter);

        this.schoolMatterService.get(schhoolMatterId).subscribe((res) => {
          console.log(schhoolMatterId);
          console.log(res);
          this.selectedSchoolMatterItem = res;
          console.log(this.selectedSchoolMatterItem);
          //this.selectedSchoolYearItems
          form.append('schoolMatter', this.selectedSchoolMatterItem.id);

          this.testService.create(form).subscribe(
            (res: any) => {
              console.log(res);
              this.modalService.dismissAll();
              //this.translate.instant('HOME.TITLE')
              AlertNotif.finish(
                'Nouvel ajout',
                'ajout effectué avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              if (err.error.detail != null) {
                AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
              } else {
                AlertNotif.finish(
                  'Nouvel ajout',
                  'Erreur, Verifiez que vous avez une bonne connexion internet',
                  'error'
                );
              }
            }
          );
        });
      });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.testService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    console.log(value);
    console.log(value.id);
    const form = new FormData();
    if (this.fichier && this.fichier.name) {
      form.append('file', this.file, this.file.name);
    }

    if (this.fichier && this.fichier.name) {
      form.append('correction', this.fichier, this.fichier.name);
    }
    form.append('name', value.testName);
    form.append('duration', value.testDuration);
    form.append('type', this.selectedtestTypeItems);
    form.append('period', this.selectedtestPeriodIItems);
    form.append('level', this.selectedtestLevelItems);

    if (this.changes === 'trimestre') {
      form.append('numPeriod', this.selectedtestPeriodTriItems);
    } else if (this.changes === 'semestre') {
      form.append('numPeriod', this.selectedtestPeriodSemItems);
    }
    console.log(this.selectedSchoolItems);
    form.append('schoolYear', this.selectedSchoolYearItems);
    console.log(this.selectedSchoolMatterItems);
    form.append('schoolMatter', this.selectedSchoolMatterItems);

    this.testService.update_partial(form, value.id).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvelle modification', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  removeFile() {
    this.file = undefined;
    this.urls = undefined;
  }

  onFilesAdded(event) {
    if (event.target.files.length > 0) {
      this.uploadResponse.status = 'progress';
      this.file = event.target.files[0];
      console.log(this.file);
      this.uploadResponse.filePath = this.urls;
      this.progress = Math.round((100 * this.file.size) / this.file.size);
      console.log(this.progress);
      return this.progress;
    }

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;

        console.log([event.target, reader.result]);
      };
    } else {
      return this.err;
    }
  }

  onFilesAddedC(event) {
    if (event.target.files.length > 0) {
      this.uploadCResponse.status = 'progress';
      this.fichier = event.target.files[0];
      console.log(this.fichier);
      this.uploadCResponse.filePath = this.lien;
      this.progressC = Math.round((100 * this.file.size) / this.file.size);
      console.log(this.progressC);
      return this.progressC;
    }

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.lien = event.target;
        console.log([event.target, reader.result]);
      };
    } else {
      return this.err;
    }
  }
}

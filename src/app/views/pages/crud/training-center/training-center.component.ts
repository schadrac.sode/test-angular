import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertNotif } from 'src/app/alert';

import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

import { TrainingCenterService } from 'src/app/core/_services/training-center.service';
import { TrainingCenter } from 'src/app/core/_models/training-center.model';

import { Training } from 'src/app/core/_models/training.model';
import { TrainingService } from 'src/app/core/_services/training.service';

import { Center } from 'src/app/core/_models/center.model';
import { CenterService } from 'src/app/core/_services/center.service';

@Component({
  selector: 'app-training-center',
  templateUrl: './training-center.component.html',
  styleUrls: ['./training-center.component.css'],
})
export class TrainingCenterComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected: any;
  collectionSize = 0;
  selected_data = new TrainingCenter();
  file: File;
  urls;

  selectedTraining: any;
  selectedCenter: any;

  selectedTrainings: any;
  selectedType: any;
  selectedCenters: any;

  centers = [];
  types = [
    {
      id: 'presentiel',
      name: 'Présentiel',
    },
    {
      id: 'numerique',
      name: 'Numérique',
    },
  ];
  trainings = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.center.name.toString().includes(term) ||
        r.training.name.toString().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString().toLowerCase();
      return el.deleted.toString().toLowerCase().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }
  filterByCenter(event) {
    console.log(event);
    if (event !== null) {
      this.trainingCenterService.getAll().subscribe((res) => {
        this.data = [];
        res.forEach((val) => {
          if (val.center.id === event) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.trainingCenterService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openEditModal(content, el) {
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //console.log(this.selected_data);
    this.selectedCenters=el.center;
    this.selectedTrainings=el.training;
    this.selectedType=el.type
    this.selected_data.id = el.id;
    this.selected_data.training = el.training;
    this.selected_data.center = el.center;
    this.selected_data.type = el.type;
    this.selected_data.infos = el.infos;
    this.selected_data.price = el.price;

    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private centerService: CenterService,
    private trainingService: TrainingService,
    private trainingCenterService: TrainingCenterService
  ) {
    this.centerService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.centers = res;
      console.log(this.centers);
    });

    this.trainingService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.trainings = res;
      console.log(this.trainings);
    });
  }

  ngOnInit(): void {
    this.collectionSize = this.data.length;
    this.trainingCenterService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      // console.log(res);
      // this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    console.log(value);
    console.log([this.selectedCenters, this.selectedTrainings]);
    this.trainingService.get(value.selectedTraining).subscribe((res: any) => {
      this.selectedTraining = res;
      console.log(this.selectedTraining);

      this.centerService.get(value.selectedCenter).subscribe((res: any) => {
        this.selectedCenter = res;
        console.log(this.selectedCenter);
      });

      const val = {
        center: value.selectedCenter,
        training: value.selectedTraining,
        type: value.selectedType,
        price: value.price,
        infos: value.infos,
      };

      this.trainingCenterService.create(val).subscribe(
        (res: any) => {
          console.log(res);
          this.modalService.dismissAll();
          //this.translate.instant('HOME.TITLE')
          AlertNotif.finish(
            'Nouvel ajout',
            'Ajout effectué avec succès',
            'success'
          );
          this.ngOnInit();
        },
        (err) => {
          if (err.error.detail != null) {
            AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
          } else {
            AlertNotif.finish(
              'Nouvel ajout',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        }
      );
    });
  }

  edit(value) {
    console.log(value);
    this.trainingService.get(value.selectedTraining).subscribe((res: any) => {
      this.selectedTraining = res;
      console.log(this.selectedTraining);

      this.centerService.get(value.selectedCenter).subscribe((res: any) => {
        this.selectedCenter = res;
        console.log(this.selectedCenter);
      });

      const val = {
        center: value.selectedCenter,
        training: value.selectedTraining,
        type: value.selectedType,
        price: value.price,
        infos: value.infos,
      };

      this.trainingCenterService.update(val, value.id).subscribe(
        (res: any) => {
          console.log(res);
          this.modalService.dismissAll();
          //this.translate.instant('HOME.TITLE')
          AlertNotif.finish(
            'Nouvelle modification',
            'Modification effectué avec succès',
            'success'
          );
          this.ngOnInit();
        },
        (err) => {
          if (err.error.detail != null) {
            AlertNotif.finish(
              'Nouvelle modification',
              err.error.detail,
              'error'
            );
          } else {
            AlertNotif.finish(
              'Nouvelle modification',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        }
      );
    });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        console.log(id);
        this.trainingCenterService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock(id, index) {
    AlertNotif.finishConfirm(
      'Suspension',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.trainingCenterService.delete_partial(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suspension',
              'Suspension effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Suspension, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  // edit(value) {

  //   console.log(value);
  //   this.trainingService.get(value.selectedTraining)
  //   .subscribe((res: any) => {
  //     this.selectedTraining=res;
  //     console.log(this.selectedTraining);
  //   });

  //   this.centerService.get(value.selectedCenter)
  //   .subscribe((res: any) => {
  //     this.selectedCenter=res;
  //     console.log(this.selectedCenter);
  //   });

  //   const val = {
  //     center: this.selectedCenters,
  //     training: this.selectedTrainings
  //   }

  //   this.trainingCenterService.update(val,value.id).subscribe((res: any) => {
  //     console.log(res)
  //     this.modalService.dismissAll()
  //     //this.translate.instant('HOME.TITLE')
  //     AlertNotif.finish("Nouvel ajout", "Ajout effectué avec succès", 'success')
  //     this.ngOnInit()
  //   }, (err) => {

  //     if (err.error.detail != null) {
  //       AlertNotif.finish("Nouvel ajout", err.error.detail, 'error')
  //     } else {
  //       AlertNotif.finish("Nouvel ajout",
  //         "Erreur, Verifiez que vous avez une bonne connexion internet", 'error')
  //     }
  //   })
  // }
}

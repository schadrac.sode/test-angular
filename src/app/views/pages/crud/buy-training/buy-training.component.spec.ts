import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyTrainingComponent } from './buy-training.component';

describe('BuyTrainingComponent', () => {
  let component: BuyTrainingComponent;
  let fixture: ComponentFixture<BuyTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseFileService } from 'src/app/core/_services/course-file.service';
import { CourseService } from 'src/app/core/_services/course.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { CourseFile } from 'src/app/core/_models/course-file.model';
import { Course } from 'src/app/core/_models/course.model';

@Component({
  selector: 'app-course-file',
  templateUrl: './course-file.component.html',
  styleUrls: ['./course-file.component.css'],
})
export class CourseFileComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new CourseFile();
  file: File;
  urls;
  courses: Course[] = [];
  selectedCourse = new Course();
  courseName: any;
  uploadResponse = { status: '', message: '', filePath: '' };
  progress: number = 0;
  err={
    message:'Erreur'
  };
  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.id = el.id;
    this.selected_data.file = el.file;
    this.selected_data.type = el.type;
    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  selectedcourseFIleTypeItems: any;
  courseFIleTypeItems = [
    {
      id: 'pdf',
      name: 'Document PDF',
    },
    {
      id: 'video',
      name: 'Vidéo',
    },
  ];

  constructor(
    private modalService: NgbModal,
    private courseFileService: CourseFileService,
    private courseService: CourseService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.courseService.getAll().subscribe((res) => {
      console.log(res);
      this.courses = res;
    });
  }
  course_id=""
  course=new Course()

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.course_id = this.activatedRoute.snapshot.paramMap.get('course');
    this.courseService.get(this.course_id).subscribe((res: any) => {
      this.course = res;
    });
    this.courseFileService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    const form = new FormData();
    form.append('name', value.courseFileName);
    form.append('course', this.course_id);
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
      if (this.file.type === 'video/mp4') {
        form.append('type', 'video');
      } else {
        form.append('type', 'pdf');
      }
    }
    this.courseFileService.create(form).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        this.ngOnInit();

        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.courseFileService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    const form = new FormData();
    form.append('name', value.courseFileName);
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
      if (this.file.type === 'video/mp4') {
        form.append('type', 'video');
      } else {
        form.append('type', 'pdf');
      }
    }
    this.courseFileService.update_partial(form, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
        this.modalService.dismissAll();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }

  removeFile() {
    this.file = undefined;
    this.urls = undefined;
  }

  onFilesAdded(event) {
    if (event.target.files.length > 0) {
      this.uploadResponse.status="progress"
      this.file = event.target.files[0];
      console.log(this.file);
      this.uploadResponse.filePath=this.urls;
      this.progress = Math.round(100 * this.file.size / this.file.size);
      console.log(this.progress);
      return this.progress;
    } 
    /*      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
*/
    // //console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;
       

        console.log([event.target, reader.result]);
      };

  

    }
    else {
      return this.err;
    }
  }

}

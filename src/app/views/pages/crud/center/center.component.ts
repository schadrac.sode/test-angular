import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { CenterService } from 'src/app/core/_services/center.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Center } from 'src/app/core/_models/center.model';
import { Training } from 'src/app/core/_models/training.model';
@Component({
  selector: 'app-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.css'],
})
export class CenterComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Center();
  file: File;
  urls;

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    ////console.log(el)
    ////console.log(content)
    //this.selected_data;
    //  //console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.id = el.id;
    this.selected_data.logo = el.logo;
    this.selected_data.sigle = el.sigle;
    //console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private centerService: CenterService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this._temp = this.data;
    this.collectionSize = this.data.length;
    this.centerService.getAll().subscribe((res: any) => {
      ////console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    ////console.log(value)
    const val = { name: value.centerName };
    const form = new FormData();
    form.append('name', value.centerName);
    form.append('sigle', value.centerSigle);
    if (this.file && this.file.name) {
      form.append('logo', this.file, this.file.name);
    }
    this.centerService.create(form).subscribe(
      (res: any) => {
        //console.log(res)

        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.centerService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
  lock(id, index) {
    AlertNotif.finishConfirm(
      'Suspension',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.centerService.delete_partial(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suspension',
              'Suspension effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
  edit(value) {
    const val = { name: value.centerName };
    const form = new FormData();
    form.append('name', value.centerName);
    form.append('sigle', value.centerSigle);
    this.centerService.update_partial(form, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Motification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }

  removeLogo() {
    this.file = undefined;
    this.urls = undefined;
  }

  onFilesAdded(event) {
    // //console.log(event.target.files);
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }

    // //console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;
        //console.log([event.target, reader.result]);
      };
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolTrainingOptionComponent } from './school-training-option.component';

describe('SchoolTrainingOptionComponent', () => {
  let component: SchoolTrainingOptionComponent;
  let fixture: ComponentFixture<SchoolTrainingOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolTrainingOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolTrainingOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';

import { SchoolTrainingOption } from 'src/app/core/_models//school-training-option.model';
import { SchoolTrainingOptionService } from 'src/app/core/_services/school-training-option.service';

import { School } from 'src/app/core/_models//school.model';
import { SchoolService } from 'src/app/core/_services/school.service';
import { TrainingOption } from 'src/app/core/_models/training-option.model';
import { TrainingOptionService } from 'src/app/core/_services/training-option.service';

@Component({
  selector: 'app-school-training-option',
  templateUrl: './school-training-option.component.html',
  styleUrls: ['./school-training-option.component.css'],
})
export class SchoolTrainingOptionComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new SchoolTrainingOption();

  selectedTrainingOption: any;
  selectedSchool: any;

  selectedTrainingsOptions: TrainingOption;
  selectedSchools: School;

  schools: School[] = [];
  trainingOptions: TrainingOption[] = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.trainingOption.name.toString().toLowerCase().includes(term) ||
        r.school.name.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString().toLowerCase();
      return el.deleted.toString().toLowerCase().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    // console.log(el);
    // console.log(el.id);
    // this.selected_data.id = el.id;
    // console.log('Ellement' + el);
    this.selectedTrainingsOptions=el.trainingOption;
    this.selectedSchools=el.school;
    this.selected_data = el;
    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private trainingOptionService: TrainingOptionService,
    private schoolTrainingOptionService: SchoolTrainingOptionService,
    private schoolService: SchoolService
  ) {
    this.trainingOptionService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.trainingOptions = res;
      console.log(this.trainingOptions);
    });

    this.schoolService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.schools = res;
      console.log(this.schools);
    });
  }

  school_id=""
  school:School=new School()
  ngOnInit() {
    this._temp = [];
    this.data = [];

    this._temp = this.data;
    this.collectionSize = this.data.length;
    this.school_id=this.activatedRoute.snapshot.paramMap.get('school');
    this.schoolService.get(this.school_id).subscribe((res: any) => {
      this.school = res;
    });
    this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
      this.spinner.hide();
      res.forEach((f) => {
        if(this.school_id==f.school.id){
          this.data.push(f)
        }
      })
      this.data = res;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    const val = {
      trainingOption: value.trainingOption,
      school: this.school_id,
    };
    console.log(val);
    this.schoolTrainingOptionService.create(val).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  edit(value) {
      this.schoolTrainingOptionService.update_partial({
        trainingOption: value.trainingOption
      }, value.id).subscribe(
        (res: any) => {
          console.log(res);
          this.modalService.dismissAll();
          this.ngOnInit();
          AlertNotif.finish(
            'Nouvel modification',
            'Modification effectué avec succès',
            'success'
          );
        },
        (err) => {
          if (err.error.detail != null) {
            AlertNotif.finish('Nouvel modification', err.error.detail, 'error');
          } else {
            AlertNotif.finish(
              'Nouvel modification',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        }
      );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression definitive',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.schoolTrainingOptionService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock_schoolTrainingOption(id, index) {
    AlertNotif.finishConfirm(
      "Suspension de l'offre",
      ' Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.schoolTrainingOptionService.delete_partial(id).subscribe(
          (res: any) => {
            // this.data.splice(index,1)
            AlertNotif.finish(
              'Suspension',
              'Suspension effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
}

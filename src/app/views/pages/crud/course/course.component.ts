import { Course } from './../../../../core/_models/course.model';
import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { CenterService } from 'src/app/core/_services/center.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { TrainingCenterService } from 'src/app/core/_services/training-center.service';
import { CourseService } from 'src/app/core/_services/course.service';
import { TrainingCenter } from 'src/app/core/_models/training-center.model';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
})
export class CourseComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Course();
  trainingCenter = new TrainingCenter();
  selectedItems: any;
  selectedCenterItems: any;
  items = [];
  centerItems = [];
  events: Event[] = [];
  centerSelected = false;

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.name.toString().toLowerCase().includes(term) ||
        r.author.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    ////console.log(el)
    ////console.log(content)
    this.selected_data = el;
    //  //console.log(this.selected_data);
    // this.selected_data.id = el.id;
    // this.selected_data.author = el.author;
    // this.selected_data.description = el.description;
    // this.selected_data.duration = el.duration;
    // this.selected_data.status = el.status;
    // this.selected_data.name = el.name;
    //console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private courseService: CourseService,
    private centerService: CenterService,
    private trainingCenterService: TrainingCenterService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.centerService.getAll().subscribe((res: any) => {
      ////console.log(res[0]);
      this.centerItems = res;
      //console.log(this.centerItems);
    });

    this.trainingCenterService.getAll().subscribe((res: any) => {
      ////console.log(res[0]);
      this.items = res;
      //console.log(this.items);
    });
  }

  training_center_id=""
  training_center=new TrainingCenter({})
  ngOnInit(): void {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    
    this.training_center_id = this.activatedRoute.snapshot.paramMap.get('training_center');
    this.trainingCenterService.get(this.training_center_id).subscribe((res: any) => {
      this.training_center = res;
    });
    this.courseService.getAll().subscribe((res: any) => {
      ////console.log(res);
      this.spinner.hide();
      res.forEach((f) => {
        if (this.training_center_id == f.trainingCenter.id) {
          this.data.push(f)
        }
      })
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    const val = {
      name: value.courseName,
      description: value.courseDescription,
      duration: value.courseDuration,
      author: value.courseAuthor,
      trainingCenter: this.training_center_id,
    };
    this.courseService.create(val).subscribe(
      (res: any) => {
        //console.log(res)

        this.modalService.dismissAll();
        this.ngOnInit();

        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  onScoolSelected(event) {
    this.centerSelected = true;
    //console.log(event);
    const id = event.id;
    this.items = [];
    this.trainingCenterService.getAll().subscribe((res: any) => {
      //console.log(res)
      let t = this;
      res.forEach((el) => {
        if (id === el.center.id) {
          t.items.push(el);
        }
        //console.log(this.items);
      });
    });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.courseService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    const val = {
      name: value.courseName,
      description: value.courseDescription,
      duration: value.courseDuration,
      author: value.courseAuthor,
    };
    //console.log(val);
    this.courseService.update_partial(val, value.id).subscribe(
      (res: any) => {
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        this.ngOnInit();

        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish(
            'Nouvelle modification',
            err.error.detail,
            'error'
          );
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }
}

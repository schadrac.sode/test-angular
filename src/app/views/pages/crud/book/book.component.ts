import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/core/_services/book.service';
import { SchoolService } from 'src/app/core/_services/school.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Book } from 'src/app/core/_models/book.model';
import { School } from 'src/app/core/_models/school.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  @Input() cssClasses = '';
  uploadResponse = { status: '', message: '', filePath: '' };

  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  err = {
    message: 'Erreur',
  };
  progress: number = 0;

  data: any[] = [];
  _temp: any[] = [];

  selected: any;
  collectionSize = 0;
  selected_data = new Book();
  file: File;
  urls;
  cover: File;
  coverUrl;
  selectedSchool: any;
  selectedItems: any;
  items = [];

  selectedType: any;
  types = [
    {
      id: 'papier',
      name: 'Papier',
    },
    {
      id: 'numerique',
      name: 'Numerique',
    },
  ];
  events: Event[] = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    // this.selectedSchool = el;
    // this.selectedType = el.type;
    console.log(el);
    ////console.log(content)
    //this.selected_data;
    ////console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.id = el.id;
    this.selected_data.edition = el.edition;
    this.selected_data.school = el.school;
    this.selectedSchool = el.school;
    this.selectedType = el.type;
    this.selected_data.price = el.price;
    this.selected_data.type = el.type;
    this.selected_data.author = el.author;
    this.selected_data.file = el.file;
    this.selected_data.cover = el.cover;
    // this.selected_data.school = el.school;

    //console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  removeLogo() {
    this.cover = undefined;
    this.coverUrl = undefined;
  }

  onFilesAdded(event) {
    if (event.target.files.length > 0) {
      this.uploadResponse.status = 'progress';
      this.file = event.target.files[0];
      console.log(this.file);
      this.uploadResponse.filePath = this.urls;
      this.progress = Math.round((100 * this.file.size) / this.file.size);
      console.log(this.progress);
      return this.progress;
    }
    /*      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
*/
    // //console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;

        console.log([event.target, reader.result]);
      };
    } else {
      return this.err;
    }
  }

  onCoverAdded(event) {
    // //console.log(event.target.files);
    if (event.target.files.length > 0) {
      this.cover = event.target.files[0];
      this.uploadResponse.status = 'progress';
      this.uploadResponse.message = 'Ajout en cours';
    }

    // //console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.coverUrl = event.target;
        //console.log([event.target, reader.result]);
      };
    }
  }
  constructor(
    private modalService: NgbModal,
    private bookService: BookService,
    private schoolService: SchoolService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.schoolService.getAll().subscribe((res: any) => {
      ////console.log(res[0]);
      this.items = res;
      // //console.log(this.items);
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.schoolService.getAll().subscribe((res) => {
      this.items = res;
    });
    this.bookService.getAll().subscribe((res: any) => {
      //console.log(res);
      this.spinner.hide();
      // //console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }
  /**
   *
   * @param year  string,
   * @param page :number,
   * @param name :string,
   * @param author:string,
   * @param school :School,
   * @param edition :string,
   * @param file: string,
   * @param cover: string,
   * @param deleted:boolean,
   */
  create(value) {
    //console.log(value);
    // this.selected=this.schoolService.get(value.bookSchool).subscribe((res: any) => {
    //   this.selectedSchool=res;
    //   //console.log(this.selectedSchool);

    const val = {
      year: value.bookYear,
      page: value.bookPage,
      name: value.bookName,
      author: value.bookAuthor,
      edition: value.bookEdition,
      cover: value.bookCover,
      file: value.bookFile,
      price: value.price,
    };
    const form = new FormData();
    form.append('year', value.bookYear);
    form.append('page', value.bookPage);
    form.append('name', value.bookName);
    form.append('author', value.bookAuthor);
    form.append('edition', value.bookEdition);
    form.append('type', this.selectedType);
    form.append('school', this.selectedSchool);
    form.append('price', value.bookPrice);
    if (this.cover && this.cover.name) {
      form.append('cover', this.cover, this.cover.name);
    }
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
    }
    this.bookService.create(form).subscribe(
      (res: any) => {
        //console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );

    // });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.bookService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    //console.log(value);
    // this.selected=this.schoolService.get(value.bookSchool).subscribe((res: any) => {
    //   this.selectedSchool=res;
    //   //console.log(this.selectedSchool);

    const val = {
      year: value.bookYear,
      page: value.bookPage,
      name: value.bookName,
      author: value.bookAuthor,
      edition: value.bookEdition,
      cover: value.bookCover,
      file: value.bookFile,
      price: value.price,
    };
    const form = new FormData();
    form.append('year', value.bookYear);
    form.append('page', value.bookPage);
    form.append('name', value.bookName);
    form.append('author', value.bookAuthor);
    form.append('edition', value.bookEdition);
    form.append('school', this.selectedSchool);
    form.append('type', this.selectedType);
    form.append('price', value.bookPrice);
    if (this.cover && this.cover.name) {
      form.append('cover', this.cover, this.cover.name);
    }
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
    }
    //console.log(value.bookId);
    this.bookService.update(form, value.id).subscribe(
      (res: any) => {
        //console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvelle modification', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );

    // });
  }
}

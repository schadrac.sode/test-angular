import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Offer } from 'src/app/core/_models/offer.model';
import { OfferService } from 'src/app/core/_services/offer.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css'],
})
export class OfferComponent implements OnInit {
  // id:number;
  // name:string;
  // description:string;
  // price:number;
  // duration:number;
  // status:boolean;

  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];
  items = [
    {
      id: 'Standard',
      name: 'Standard',
    },
    {
      id: 'Epreuve et Correction',
      name: 'Epreuve et Correction',
    },
    {
      id: 'Tp et Rapport',
      name: 'Tp et Rapport',
    },
    {
      id: 'Mémoire',
      name: 'Mémoire',
    },
  ];
  selected = [];
  selectedModule : any;
  collectionSize = 0;
  selected_data = new Offer();

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
   //  console.log(el);
    //console.log(content)
    //this.selected_data;
    //  console.log(this.selected_data);
    this.selected_data = el;
    // console.log(this.selected_data);

    this.selectedModule = this.selected_data.module;
    console.log(this.selectedModule);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private offerService: OfferService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.offerService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    console.log(value);
    let data = [];
    // if (this.selectedModule && this.selectedModule.length !== 0) {
    //   this.selectedModule.forEach((module) => {
    //     data.push(JSON.stringify(module.id));
    //   });
    // }
    const val = {
      name: value.offerName,
      description: value.offerDescription,
      price: value.offerPrice,
      duration: value.offerDuration,
      module: this.selectedModule,
    };
    console.log(this.selectedModule);
    this.offerService.create(val).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.offerService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    console.log(value);
    const val = {
      name: value.offerName,
      description: value.offerDescription,
      price: value.offerPrice,
      duration: value.offerDuration,
      module: this.selectedModule,
    };
    console.log(this.selectedModule);
    this.offerService.update(val, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
        this.modalService.dismissAll();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }
  lock_offer(id, index) {
    AlertNotif.finishConfirm('Suspension', ' Voulez-vous continuer ?').then(
      (result) => {
        if (result.value) {
          this.offerService.delete_partial(id).subscribe(
            (res: any) => {
              // this.data.splice(index,1)
              AlertNotif.finish(
                'Suspension',
                'Suspension effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              AlertNotif.finish(
                'Suspension',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          );
        }
      }
    );
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertNotif } from 'src/app/alert';

import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

import { SchoolMatter } from 'src/app/core/_models/school-matter.model';
import { Matter } from 'src/app/core/_models/matter.model';
import { SchoolTrainingOption } from 'src/app/core/_models/school-training-option.model';

import { SchoolTrainingOptionComponent } from '../school-training-option/school-training-option.component';
import { MatterComponent } from '../matter/matter.component';

import { SchoolMatterService } from 'src/app/core/_services/school-matter.service';
import { SchoolTrainingOptionService } from 'src/app/core/_services/school-training-option.service';
import { MatterService } from 'src/app/core/_services/matter.service';

@Component({
  selector: 'app-school-matter',
  templateUrl: './school-matter.component.html',
  styleUrls: ['./school-matter.component.css'],
})
export class SchoolMatterComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected: any;
  collectionSize = 0;
  selected_data = new SchoolMatter();
  file: File;
  urls;

  selectedSchoolTrainingOption: any;
  selectedSchoolMatter: any;

  selectedSchoolTrainingsOptions: SchoolTrainingOption;
  selectedMatters: Matter;

  matters: Matter[] = [];
  schoolTrainingsOptions: SchoolTrainingOption[] = [];

  search() {
    this.data = this._temp.filter((el) => {
      const term = this.searchText.toLowerCase();
      return (
        el.matter.name.toString().toLowerCase().includes(term) ||
        el.schoolTrainingOption.trainingOption.name
          .toString()
          .toLowerCase()
          .includes(term)
      );
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value);
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString().toLowerCase();
      return el.deleted.toString().toLowerCase().toLowerCase().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    this.selectedMatters=el.matter;
    this.selectedSchoolTrainingsOptions=el.schoolTrainingOption;
    this.selected_data = el;
    //console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private schoolMatterService: SchoolMatterService,
    private matterService: MatterService,
    private schoolTrainingOptionService: SchoolTrainingOptionService
  ) {
    this.matterService.getAll().subscribe((res: any) => {
      ////console.log(res[0]);
      this.matters = res;
      //console.log(this.matters);
    });

    this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
      ////console.log(res[0]);
      this.schoolTrainingsOptions = res;
      //console.log(this.schoolTrainingsOptions);
    });
  }

  school_training_option_id = '';
  school_training_option: SchoolTrainingOption = new SchoolTrainingOption({});
  ngOnInit(): void {
    this.collectionSize = this.data.length;
    this.school_training_option_id = this.activatedRoute.snapshot.paramMap.get(
      'school_training_option'
    );
    this.schoolTrainingOptionService
      .get(this.school_training_option_id)
      .subscribe((res: any) => {
        this.school_training_option = res;
      });
    this.schoolMatterService.getAll().subscribe((res: any) => {
      this.spinner.hide();
      res.forEach((f) => {
        if (this.school_training_option_id == f.schoolTrainingOption.id) {
          this.data.push(f);
        }
      });
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    value.schoolTrainingOption = this.school_training_option_id;
    this.schoolMatterService.create(value).subscribe(
      (res: any) => {
        this.modalService.dismissAll();
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  edit(value) {
    this.schoolMatterService.update_partial(value, value.id).subscribe(
      (res: any) => {
        this.modalService.dismissAll();
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvelle modification', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.schoolMatterService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock(id, index) {
    AlertNotif.finishConfirm(
      'Suspension',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.schoolMatterService.delete_partial(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suspension',
              'Suspension effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Suspension, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
}

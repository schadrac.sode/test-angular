import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolMatterComponent } from './school-matter.component';

describe('SchoolMatterComponent', () => {
  let component: SchoolMatterComponent;
  let fixture: ComponentFixture<SchoolMatterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolMatterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolMatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

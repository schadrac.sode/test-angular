import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { PayementSettings } from 'src/app/core/_models/payement-settings.model';
import { PayementSettingsService } from 'src/app/core/_services/payement-settings.service';

@Component({
  selector: 'app-payement-settings',
  templateUrl: './payement-settings.component.html',
  styleUrls: ['./payement-settings.component.css'],
})
export class PayementSettingsComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new PayementSettings();


  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }


  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //  console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.numero = el.numero;
    this.selected_data.id = el.id;
    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private payementSettingsService: PayementSettingsService
  ) {}

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.payementSettingsService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    const val = { name: value.settingName, numero: value.settingNumber };
    console.log(val);

    this.payementSettingsService.create(val).subscribe(
      (res: any) => {
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression definitive',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.payementSettingsService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  activate(ressource, id, index) {
    console.log(ressource);
    let activatedRessource = ressource;
    activatedRessource.status = true;

    AlertNotif.finishConfirm('Activation', ' Voulez-vous continuer ?').then(
      (result) => {
        if (result.value) {
          this.payementSettingsService
            .update_partial(activatedRessource, id)
            .subscribe(
              (res: any) => {
                // this.data.splice(index,1)
                AlertNotif.finish(
                  'Suspension',
                  'Suspensionssion effectuée avec succès',
                  'success'
                );
                this.ngOnInit();
              },
              (err) => {
                AlertNotif.finish(
                  'Suspension',
                  'Erreur, Verifiez que vous avez une bonne connexion internet',
                  'error'
                );
              }
            );
        }
      }
    );
  }
  //  ée unlock(ressource, id, index) {

  unActivate(ressource, id, index) {
    console.log(ressource);
    let unActivatedRessource = ressource;
    unActivatedRessource.status = false;

    AlertNotif.finishConfirm(
      'Désactivation',
      ' Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.payementSettingsService
          .update_partial(unActivatedRessource, id)
          .subscribe(
            (res: any) => {
              // this.data.splice(index,1)
              AlertNotif.finish(
                'Désactivation',
                'Suspensionssion effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              AlertNotif.finish(
                'Désactivation',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          );
      }
    });
  }

  edit(value) {
    const val = { name: value.settingName, numero: value.settingNumber };
    this.payementSettingsService.update_partial(val, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }
}

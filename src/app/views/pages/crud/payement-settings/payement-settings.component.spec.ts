import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayementSettingsComponent } from './payement-settings.component';

describe('DomainComponent', () => {
  let component: PayementSettingsComponent;
  let fixture: ComponentFixture<PayementSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayementSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayementSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpAndReportComponent } from './tp-and-report.component';

describe('TpAndReportComponent', () => {
  let component: TpAndReportComponent;
  let fixture: ComponentFixture<TpAndReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpAndReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpAndReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

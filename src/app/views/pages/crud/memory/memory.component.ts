import { SchoolYear } from './../../../../core/_models/school-year.model';
import { SchoolYearService } from './../../../../core/_services/school-year.service';
import { DomainService } from './../../../../core/_services/domain.service';
import { SchoolService } from './../../../../core/_services/school.service';
import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { MemoryService } from 'src/app/core/_services/memory.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Memory } from 'src/app/core/_models/memory.model';
import { Domain } from 'domain';
import { School } from 'src/app/core/_models/school.model';

@Component({
  selector: 'app-memory',
  templateUrl: './memory.component.html',
  styleUrls: ['./memory.component.css'],
})
export class MemoryComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Memory();
  file: File;
  urls;
  uploadResponse = { status: '', message: '', filePath: '' };
  progress: number = 0;
  err = {
    message: 'Erreur',
  };

  domainItems = [];
  selectedDomainItems: any;

  schoolItems = [];
  selectedSchoolItems: any;

  schoolYearitems = [];
  selectedSchoolYearItems: any;

  selectedDomain: any;
  selectedSchool: any;

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return;
      r.subject.toString().toLowerCase().includes(term) ||
        r.author.toString().toLowerCase().includes(term) ||
        r.school.name.toString().toLowerCase().includes(term) ||
        r.domain.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  filterBySchool(event) {
    console.log(event);
    if (event !== null) {
      this.memoryService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.school.id === event) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.memoryService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  filterByDomain(event) {
    console.log(event);
    if (event !== null) {
      this.memoryService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.domain.id === event) {
            this.data.push(val);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.memoryService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    this.selectedSchoolItems = el.school.id;
    this.selectedDomainItems = el.domain.id;
    this.selectedSchoolYearItems = el.schoolYear.id;

    this.selected_data.subject = el.subject;
    this.selected_data.id = el.id;
    if (el.file) {
      this.selected_data.file = el.file;
    }
    this.selected_data.author = el.author;
    this.selected_data.domain = el.domain;
    this.selected_data.school = el.school;
    this.selected_data.schoolYear = el.schoolYear;

    console.log(this.selected_data);

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private memoryService: MemoryService,
    private schoolService: SchoolService,
    private domaineService: DomainService,
    private schoolYearService: SchoolYearService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.memoryService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
    this.schoolYearService.getAll().subscribe((res: any) => {
      res.forEach((el: SchoolYear) => {
        el.academie = `${el.start}-${el.end}`;
        this.schoolYearitems.push(el);
        console.log(this.schoolYearitems);
      });
    });
    this.schoolService.getAll().subscribe((res: any) => {
      console.log(res);
      this.schoolItems = res;
    });
    this.domaineService.getAll().subscribe((res: any) => {
      console.log(res);
      this.domainItems = res;
    });
  }

  create(value) {
    //console.log(value)
    const val = {
      subject: value.memorySubject,
      author: value.memoryAuthor,
      domain: value.memoryDomain,
      school: value.memorySchool,
      schoolYear: value.memorySchoolYear,
    };
    const form = new FormData();
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
    }
    form.append('subject', value.memorySubject);
    form.append('schoolYear', this.selectedSchoolYearItems.id);
    form.append('author', value.memoryAuthor);
    form.append('domain', value.memoryDomain);
    form.append('school', value.memorySchool);
    this.memoryService.create(form).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.memoryService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    const form = new FormData();
    if (this.file && this.file.name) {
      form.append('file', this.file, this.file.name);
    }
    console.log(this.selectedSchoolYearItems);
    form.append('subject', value.memorySubject);
    form.append('schoolYear', this.selectedSchoolYearItems);
    form.append('author', value.memoryAuthor);
    form.append('domain', value.memoryDomain);
    form.append('school', value.memorySchool);
    this.memoryService.update(form, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
        this.modalService.dismissAll();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }

  removeFile() {
    this.file = undefined;
    this.urls = undefined;
  }

  onFilesAdded(event) {
    if (event.target.files.length > 0) {
      this.uploadResponse.status = 'progress';
      this.file = event.target.files[0];
      console.log(this.file);
      this.uploadResponse.filePath = this.urls;
      this.progress = Math.round((100 * this.file.size) / this.file.size);
      console.log(this.progress);
      return this.progress;
    }
    /*      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
*/
    // //console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;

        console.log([event.target, reader.result]);
      };
    } else {
      return this.err;
    }
  }
}

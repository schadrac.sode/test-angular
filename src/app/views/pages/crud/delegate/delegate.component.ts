import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/core/_models/user.model';
import { UserService } from 'src/app/core/_services/user.service';
import { PermissionService } from 'src/app/core/_services/permission.service';
import { Permission } from 'src/app/core/_models/permission.model';
import { Delegate } from 'src/app/core/_models/delegate.model';
import { DelegateService } from 'src/app/core/_services/delegate.service';
DelegateService;

@Component({
  selector: 'app-delegate',
  templateUrl: './delegate.component.html',
  styleUrls: ['./delegate.component.css'],
})
export class DelegateComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Delegate();

  selectedPermission: any;
  selectedUser: any;

  selectedPermissions: any;
  selectedUsers: any;

  users = [];
  permissions = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.user.username.toString().toLowerCase().includes(term) ||
        r.permission.name.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    this.selectedUsers=el.user;
    this.selectedPermissions=el.permission;
    this.selected_data.id = el.id;
    this.selected_data.user = el.user;
    this.selected_data.permission = el.permission;

    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private permissionService: PermissionService,
    private delegateService: DelegateService
  ) {
    this.userService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.users = res;
      console.log(this.users);
    });

    this.permissionService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.permissions = res;
      console.log('Permission');
      console.log(this.permissions);
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this._temp = this.data;
    this.collectionSize = this.data.length;
    this.delegateService.getAll().subscribe((res: any) => {
      //  console.log(res);
      this.spinner.hide();
      this.data = res;
      this.collectionSize = this.data.length;
    });
  }
  create(value) {
    console.log(value);
    this.permissionService
      .get(value.selectedPermission)
      .subscribe((res: any) => {
        this.selectedPermission = res;
        console.log(this.selectedPermission);

        this.userService.get(value.selectedUser).subscribe((res: any) => {
          this.selectedUser = res;
          console.log(this.selectedUser);
        });

        const val = {
          user: value.selectedUser,
          permission: value.selectedPermission,
        };

        this.delegateService.create(val).subscribe(
          (res: any) => {
            console.log(res);
            this.modalService.dismissAll();
            //this.translate.instant('HOME.TITLE')
            AlertNotif.finish(
              'Nouvel ajout',
              'Ajout effectué avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            if (err.error.detail != null) {
              AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
            } else {
              AlertNotif.finish(
                'Nouvel ajout',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          }
        );
      });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.delegateService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock(id, index) {
    AlertNotif.finishConfirm(
      'Suspension',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.delegateService.delete_partial(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suspension',
              'Suspension effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Suspension, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    console.log(value);
    this.permissionService
      .get(value.selectedPermission)
      .subscribe((res: any) => {
        this.selectedPermission = res;
        console.log(this.selectedPermission);

        this.userService.get(value.selectedUser).subscribe((res: any) => {
          this.selectedUser = res;
          console.log(this.selectedUser);
        });

        const val = {
          user: value.selectedUser,
          permission: value.selectedPermission,
        };

        this.delegateService.update(val, value.id).subscribe(
          (res: any) => {
            console.log(res);
            this.modalService.dismissAll();
            //this.translate.instant('HOME.TITLE')
            AlertNotif.finish(
              'Nouvelle modification',
              'Modification effectué avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            if (err.error.detail != null) {
              AlertNotif.finish(
                'Nouvelle modification',
                err.error.detail,
                'error'
              );
            } else {
              AlertNotif.finish(
                'Nouvelle modification',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          }
        );
      });
  }
}

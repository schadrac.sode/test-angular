import { SchoolService } from 'src/app/core/_services/school.service';
import { School } from 'src/app/core/_models/school.model';
import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { MatterService } from 'src/app/core/_services/matter.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';
import { Matter } from 'src/app/core/_models/matter.model';
import { SchoolTrainingOption } from 'src/app/core/_models/school-training-option.model';
import { SchoolTrainingOptionService } from 'src/app/core/_services/school-training-option.service';
import { TrainingOptionService } from 'src/app/core/_services/training-option.service';
import { SchoolMatterService } from 'src/app/core/_services/school-matter.service';
import { TrainingOption } from 'src/app/core/_models/training-option.model';

@Component({
  selector: 'app-matter',
  templateUrl: './matter.component.html',
  styleUrls: ['./matter.component.css'],
})
export class MatterComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Matter();
  schoolSelected = false;
  testSchoolTrainingOptionItems: SchoolTrainingOption[] = [];
  trainingOptionItems: TrainingOption[] = [];
  schoolItems: School[] = [];
  schoolTrainingOptionItems: SchoolTrainingOption;
  _matter: any;
  selectedTrainingOption: any;
  selectedSchool: any;

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().toLowerCase().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  filterBySchool(event) {
    console.log(event);
    if (event !== null) {
      this.schoolMatterService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.schoolTrainingOption.school.id === event) {
            this.data.push(val.matter);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.matterService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  filterByTrainingOption(event) {
    console.log(event);
    if (event !== null) {
      this.schoolMatterService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.schoolTrainingOption.trainingOption.id === event) {
            this.data.push(val.matter);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.matterService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //  console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.id = el.id;
    // console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  onScoolSelected(event) {
    this.schoolSelected = true;
    console.log(event);
    const id = event.id;
    this.testSchoolTrainingOptionItems = [];
    this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
      console.log(res);
      let t = this;
      res.forEach((el) => {
        if (id === el.school.id) {
          t.testSchoolTrainingOptionItems.push(el);
        }
        console.log(this.testSchoolTrainingOptionItems);
      });
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private matterService: MatterService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private schoolTrainingOptionService: SchoolTrainingOptionService,
    private trainingOptionService: TrainingOptionService,
    private schoolMatterService: SchoolMatterService,
    private schoolService: SchoolService,
    private activatedRoute: ActivatedRoute
  ) {
    // this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
    //   //console.log(res[0]);
    //   this.testSchoolTrainingOptionItems = res;
    //   // console.log('Option d formation ');
    //   // console.log('Option d formation ' + this.testSchoolTrainingOptionItems);
    // });
    this.schoolService.getAll().subscribe((res) => {
      this.schoolItems = res;
    });
    this.trainingOptionService.getAll().subscribe((res) => {
      this.trainingOptionItems = res;
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.matterService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }
  /*

    this.matterService.create(val).subscribe((res:any)=>{
      console.log(res)
      this.modalService.dismissAll()
     //this.translate.instant('HOME.TITLE')
    AlertNotif.finish("Nouvel ajout","Ajout effectué avec succès" , 'success')
      this.ngOnInit()
    },(err)=>{

      if(err.error.detail!=null){
        AlertNotif.finish("Nouvel ajout", err.error.detail, 'error')
      } else {
        AlertNotif.finish("Nouvel ajout",
        "Erreur, Verifiez que vous avez une bonne connexion internet", 'error')
      }
    })

*/

  create(value) {
    //console.log(value)
    const val = { name: value.matterName };
    this.matterService.create(val).subscribe(
      (res) => {
        this.modalService.dismissAll();
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
       
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.matterService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    const val = { name: value.matterName };
    this.matterService.update(val, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
        this.modalService.dismissAll();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }

  lock(id, index) {
    AlertNotif.finishConfirm('Suspension', ' Voulez-vous continuer ?').then(
      (result) => {
        if (result.value) {
          this.matterService.delete(id).subscribe(
            (res: any) => {
              // this.data.splice(index,1)
              AlertNotif.finish(
                'Suspension',
                'SuppreSuspensionssion effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              AlertNotif.finish(
                'Suspension',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          );
        }
      }
    );
  }
}

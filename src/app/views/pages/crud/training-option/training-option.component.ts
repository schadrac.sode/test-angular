import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertNotif } from 'src/app/alert';

import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

import { TrainingOptionService } from 'src/app/core/_services/training-option.service';
import { TrainingOption } from 'src/app/core/_models/training-option.model';

import { School } from 'src/app/core/_models/school.model';
import { SchoolService } from 'src/app/core/_services/school.service';

import { SchoolTrainingOptionService } from 'src/app/core/_services/school-training-option.service';
import { SchoolTrainingOption } from 'src/app/core/_models/school-training-option.model';

@Component({
  selector: 'app-training-option',
  templateUrl: './training-option.component.html',
  styleUrls: ['./training-option.component.css'],
})
export class TrainingOptionComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new TrainingOption();

  schoolItems: School[] = [];
  selectedSchool: any;
  _school: any;
  _trainingOption: any;
  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString().toLowerCase();
      return el.deleted.toString().toLowerCase().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  filterBySchool(event) {
    console.log(event);
    if (event !== null) {
      this.schoolTrainingOptionService.getAll().subscribe((res: any) => {
        this.data = [];
        res.forEach((val) => {
          if (val.school.id === event) {
            this.data.push(val.trainingOption);
          }
          this._temp = this.data;
          this.collectionSize = this.data.length;
          console.log(this.data);
        });
      });
    } else {
      this.trainingOptionService.getAll().subscribe((res: any) => {
        console.log(res);
        this.spinner.hide();
        this.data = res;
        this._temp = this.data;
        this.collectionSize = this.data.length;
      });
    }
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    console.log(el);
    //console.log(content)
    //this.selected_data;
    //  console.log(this.selected_data);
    this.selected_data = el;
    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private trainingOptionService: TrainingOptionService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private schoolService: SchoolService,
    private schoolTrainingOptionService: SchoolTrainingOptionService,
    private activatedRoute: ActivatedRoute
  ) {
    this.schoolService.getAll().subscribe((res) => {
      //console.log(res)
      this.schoolItems = res;
      //console.log(this.trainingOptionItems);
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this._temp = this.data;
    this.collectionSize = this.data.length;
    this.trainingOptionService.getAll().subscribe((res: any) => {
      //  console.log(res);
      this.spinner.hide();
      this.data = res;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    const val = {
      name: value.trainingOptionName,
    };
    this.trainingOptionService.create(val).subscribe(
      (trainingOption: any) => {
        this.modalService.dismissAll();
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );

        
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression  ',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.trainingOptionService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock(id, index) {
    AlertNotif.finishConfirm('Suspension ', ' Voulez-vous continuer ?').then(
      (result) => {
        if (result.value) {
          this.trainingOptionService.delete_partial(id).subscribe(
            (res: any) => {
              // this.data.splice(index,1)
              AlertNotif.finish(
                'Suspension',
                'Suspensionssion effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              AlertNotif.finish(
                'Suspension',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          );
        }
      }
    );
  }

  edit(value) {
    const val = { name: value.trainingOptionName };
    this.trainingOptionService.update_partial(val, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { BuyBookService } from 'src/app/core/_services/buy-book.service';
import { UserService } from 'src/app/core/_services/user.service';
import { BookService } from 'src/app/core/_services/book.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';

import { BuyBook } from 'src/app/core/_models/buy-book.model';
import { Book } from 'src/app/core/_models/book.model';
import { User } from 'src/app/core/_models/user.model';

@Component({
  selector: 'app-buy-book',
  templateUrl: './buy-book.component.html',
  styleUrls: ['./buy-book.component.css'],
})
export class BuyBookComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  user: any;
  book: any;
  collectionSize = 0;
  selected_data = new BuyBook();
  selectedUser: any;
  selectedBook: any;

  userItems: User[] = [];
  bookItems: Book[] = [];

  events: Event[] = [];

  /*
      return r.first_name.toLowerCase().includes(term) ||
       r.last_name.toString().includes(term) ||
       r.email.toString().includes(term)
    })
*/
  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.user.first_name.toLowerCase().toLowerCase().includes(term) ||
        r.user.username.toString().toLowerCase().includes(term) ||
        r.book.name.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    
    console.log(el);
    this.selected_data = el;
    this.selectedUser = el.user;
    this.selectedBook=el.book;
    //console.log(content)
    //this.selected_data;
    //console.log(this.selected_data);
    // this.selected_data.name = el.name;
    // this.selected_data.id = el.id;
    // this.selected_data.edition = el.edition;
    // this.selected_data.author = el.author;
    // this.selected_data.file = el.file;
    // this.selected_data.cover = el.cover;
    // this.selected_data.school = el.school;

    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  /* removeLogo() {
   this.cover = undefined;
   this.coverUrl = undefined;
  }*/

  // onFilesAdded(event) {
  //   // console.log(event.target.files);
  //   if (event.target.files.length > 0) {
  //     this.file = event.target.files[0];
  //   }

  //   // console.log(this.file);

  //   if (event.target.files && event.target.files[0]) {
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onload = (event) => {
  //       // called once readAsDataURL is completed
  //       this.urls = event.target;
  //       console.log([event.target, reader.result]);
  //     };
  //   }
  // }

  // onCoverAdded(event) {
  //   // console.log(event.target.files);
  //   if (event.target.files.length > 0) {
  //     this.cover = event.target.files[0];
  //   }

  //   // console.log(this.file);

  //   if (event.target.files && event.target.files[0]) {
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onload = (event) => {
  //       // called once readAsDataURL is completed
  //       this.coverUrl = event.target;
  //       console.log([event.target, reader.result]);
  //     };
  //   }
  // }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private buyBookService: BuyBookService,
    private bookService: BookService,
    private userService: UserService
  ) {
    this.bookService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.bookItems = res;
      console.log(this.bookItems);
    });

    this.userService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.userItems = res;
      console.log(this.userItems);
    });
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.buyBookService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      // console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    console.log(value);
    // this.selected=this.userService.get(value.bookSchool).subscribe((res: any) => {
    //   this.selectedSchool=res;
    //   console.log(this.selectedSchool);
    // const form = new FormData();
    // form.append('year', value.bookYear);
    // form.append('page', value.bookPage);
    // form.append('name', value.bookName);
    // form.append('author', value.bookAuthor);
    // form.append('edition', value.bookEdition);
    // if (this.cover && this.cover.name) {
    //   form.append('cover', this.cover, this.cover.name);
    // }
    // if (this.file && this.file.name) {
    //   form.append('file', this.file, this.file.name);
    // }
    this.userService.get(value.user).subscribe((user) => {
      console.log(user);
      this.user = user;

      this.bookService.get(value.book).subscribe((book) => {
        console.log(book);
        this.book = book;
      });

      const val = {
        price: value.price,
        user: value.user,
        book: value.book,
        refTransaction: value.refTransaction,
      };
      console.log(val);

      this.buyBookService.create(val).subscribe(
        (res: any) => {
          console.log(res);
          this.modalService.dismissAll();
          //this.translate.instant('HOME.TITLE')
          AlertNotif.finish(
            'Nouvel ajout',
            'Ajout effectué avec succès',
            'success'
          );
          this.ngOnInit();
        },
        (err) => {
          if (err.error.detail != null) {
            AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
          } else {
            AlertNotif.finish(
              'Nouvel ajout',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        }
      );
    });

    // });
  }

  edit(value) {
    console.log(value);
    // this.selected=this.userService.get(value.bookSchool).subscribe((res: any) => {
    //   this.selectedSchool=res;
    //   console.log(this.selectedSchool);
    // const form = new FormData();
    // form.append('year', value.bookYear);
    // form.append('page', value.bookPage);
    // form.append('name', value.bookName);
    // form.append('author', value.bookAuthor);
    // form.append('edition', value.bookEdition);
    // if (this.cover && this.cover.name) {
    //   form.append('cover', this.cover, this.cover.name);
    // }
    // if (this.file && this.file.name) {
    //   form.append('file', this.file, this.file.name);
    // }
    this.userService.get(value.user).subscribe((user) => {
      console.log(user);
      this.user = user;

      this.bookService.get(value.book).subscribe((book) => {
        console.log(book);
        this.book = book;
      });

      const val = {
        price: value.price,
        user: value.user,
        book: value.book,
        refTransaction: value.refTransaction,
      };
      console.log(val);

      this.buyBookService.update(val, value.id).subscribe(
        (res: any) => {
          console.log(res);
          this.modalService.dismissAll();
          //this.translate.instant('HOME.TITLE')
          AlertNotif.finish(
            'Nouvelle modification',
            'Modification effectué avec succès',
            'success'
          );
          this.ngOnInit();
        },
        (err) => {
          if (err.error.detail != null) {
            AlertNotif.finish(
              'Nouvelle modification',
              err.error.detail,
              'error'
            );
          } else {
            AlertNotif.finish(
              'Nouvelle modification',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        }
      );
    });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.buyBookService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
  unlock(data, index) {
    const dt = {
      user: data.user,
      book: data.book,
      price: data.price,
      status: true,
      refTransaction: data.refTransaction,
    };
    AlertNotif.finishConfirm(
      'Validation',
      'Voulez-vous vraiment valider ?'
    ).then((result) => {
      if (result.value) {
        this.buyBookService.update_partial(dt, data.id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Débloquage',
              'Validation effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Débloquage',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }
  lock(data, index) {
    const dt = {
      user: data.user,
      book: data.book,
      price: data.price,
      status: false,
      refTransaction: data.refTransaction,
    };
    AlertNotif.finishConfirm('Rejet', 'Voulez-vous vraiment rejeter ?').then(
      (result) => {
        if (result.value) {
          this.buyBookService.update_partial(dt, data.id).subscribe(
            (res: any) => {
              this.data.splice(index, 1);
              AlertNotif.finish(
                'Bloquage',
                'Bloquage effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              AlertNotif.finish(
                'Bloquage',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          );
        }
      }
    );
  }
}

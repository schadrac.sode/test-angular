import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertNotif } from 'src/app/alert';

import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from 'src/app/core/_services/user.service';
import { User } from 'src/app/core/_models/user.model';

import { Offer } from 'src/app/core/_models/offer.model';
import { OfferService } from 'src/app/core/_services/offer.service';

import { Subscription } from 'src/app/core/_models/subscription.model';
import { SubscriptionService } from 'src/app/core/_services/subscription.service';
import * as moment from 'moment';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbModal,
  NgbDate,
  NgbDateParserFormatter,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css'],
})
export class SubscriptionComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];

  selected: any;
  collectionSize = 0;
  selected_data = new Subscription();
  file: File;
  urls;

  // selectedTraining: any;
  // selectedCenter: any;
  /*
    selectedSchoolTrainingOption: any;
    selectedSchoolMatter: any;

    selectedSchoolTrainingsOptions: SchoolTrainingOption;
    selectedMatters: Matter;

    matters :Matter[]=[];
    schoolTrainingsOptions: SchoolTrainingOption[]=[];

  */
  selectedOffer: any;
  selectedUser: any;

  selectedOffers: [];
  selectedUsers: [];

  users = [];
  offers = [];

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.user.username.toString().toLowerCase().includes(term) ||
        r.offer.name.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openEditModal(content, el) {
    this.selectedUsers=el.user;
    this.selectedOffers=el.offer;
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //console.log(this.selected_data);
    this.selected_data.id = el.id;
    this.selected_data.user = el.user;
    this.selected_data.offer = el.offer;

    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  minDate;
  maxDate: Date;
  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null;
  toDate: NgbDate;

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private offerService: OfferService,
    private subscriptionService: SubscriptionService,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter
  ) {
    this.minDate = new Date();
    this.minDate = moment(this.minDate).format();
    ////console.log(this.minDate);

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);

    this.userService.getAll().subscribe((res: any) => {
      //console.log(res[0]);
      this.users = res;
      console.log(this.users);
    });

    this.offerService.getAll().subscribe((res: any) => {
      console.log(res);
      this.offers = res;
      console.log(this.offers);
    });
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (
      this.fromDate &&
      !this.toDate &&
      date &&
      date.after(this.fromDate)
    ) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return (
      this.fromDate &&
      !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.hoveredDate)
    );
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      (this.toDate && date.equals(this.toDate)) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed))
      ? NgbDate.from(parsed)
      : currentValue;
  }

  ngOnInit(): void {
    this.minDate = new Date();
    this.minDate = moment(this.minDate).format();
    console.log(this.minDate);
    this.collectionSize = this.data.length;
    this.subscriptionService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      // console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  create(value) {
    console.log(value.expiredAt);

    let expired = moment(value.expiredAt).format('YYYY-MM-DDTHH:mm:ss.SSS');
    console.log(expired);
    this.offerService.get(value.selectedOffer).subscribe((res: any) => {
      //console.log(res);
      this.selectedOffer = res;
      console.log(this.selectedOffer);
      this.userService.get(value.selectedUser).subscribe((user: any) => {
        // console.log(user);
        this.selectedUsers = user;
        console.log(this.selectedUsers);
        //YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]." ]
        const val = {
          user: value.selectedUser,
          offer: value.selectedOffer,
          expiredAt: expired,
          refTransaction: value.refTransaction,
        };
        this.subscriptionService.create(val).subscribe(
          (res: any) => {
            console.log(res);
            this.modalService.dismissAll();
            //this.translate.instant('HOME.TITLE')
            AlertNotif.finish(
              'Nouvel ajout',
              'Ajout effectué avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            if (err.error.detail != null) {
              AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
            } else {
              AlertNotif.finish(
                'Nouvel ajout',
                'Erreur, Verifiez que vous avez une bonne connexion internet',
                'error'
              );
            }
          }
        );
      });
    });
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.subscriptionService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  unValidate(id) {
    AlertNotif.finishConfirm(
      'Validation',
      'Voulez-vous rejeter cet abonnement ?'
    ).then((result) => {
      console.log(result.value);
      if (result.value) {
        // console.log(ressource);
        // let unValidatedRessource = ressource;
        // unValidatedRessource.validate = 'reject';
        this.subscriptionService.rejected(id).subscribe(
          (res: any) => {
            AlertNotif.finish(
              'Validation',
              'Rejet effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Validation',
              'Suspension, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  validate(id) {
    AlertNotif.finishConfirm(
      'Validation',
      'Voulez-vous valider cet abonnement ?'
    ).then((result) => {
      if (result.value) {
        this.subscriptionService.validate(id).subscribe(
          (res: any) => {
            AlertNotif.finish(
              'Validation',
              'Validation effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Validation',
              'Suspension, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    console.log(value);
    // this.offerService.get(value.selectedCenter)
    // .subscribe((res: any) => {
    //   this.selectedTraining=res;
    //   console.log(this.selectedTraining);
    // });

    // this.userService.get(value.selectedTraining)
    // .subscribe((res: any) => {
    //   this.selectedCenter=res;
    //   console.log(this.selectedCenter);
    // });
    /*
          const val = {
            user: value.selectedUser,
            offer: value.selectedOffer,
            expiredAt:value.expiredAt
            refTransaction:value.refTransaction
          }
    */

    const val = {
      user: value.selectedUser,
      offer: value.selectedOffer,
      expiredAt: value.expiredAt,
      refTransaction: value.refTransaction,
    };

    this.subscriptionService.update_partial(val, value.id).subscribe(
      (res: any) => {
        console.log(res);
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvelle modification',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvelle modification', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }
}

import { TrainingCenterService } from 'src/app/core/_services/training-center.service';
import { CenterService } from 'src/app/core/_services/center.service';
import { Component, OnInit, Input } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { SchoolService } from 'src/app/core/_services/school.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';

import { TrainingService } from 'src/app/core/_services/training.service';
import { Training } from 'src/app/core/_models/training.model';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css'],
})
export class TrainingComponent implements OnInit {
  @Input() cssClasses = '';
  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];
  _trainingCenter: any[] = [];

  selected = [];
  collectionSize = 0;
  selected_data = new Training();

  centers = [];
  selectedCenter: any;

  types = [
    {
      id: 'presentiel',
      name: 'Presentiel',
    },
    {
      id: 'numerique',
      name: 'Numérique',
    },
  ];
  selectedType: any;

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return r.name.toString().includes(term);
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString();
      return el.deleted.toString().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //console.log(el)
    //console.log(content)
    //this.selected_data;
    //  console.log(this.selected_data);
    this.selected_data.name = el.name;
    this.selected_data.id = el.id;
    console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(
    private modalService: NgbModal,
    private trainingService: TrainingService,
    private trainingCenterService: TrainingCenterService,
    private centerService: CenterService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._temp = [];
    this.data = [];

    this.collectionSize = this.data.length;
    this.trainingService.getAll().subscribe((res: any) => {
      console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
    this.centerService.getAll().subscribe((res: any) => {
      console.log(res);
      this.centers = res;
    });
  }

  create(value) {
    console.log(value);
    const val = { name: value.trainingName };
    this.trainingService.create(val).subscribe(
      (res: any) => {
        // this.trainingCenterService
        //   .create({
        //     center: this.selectedCenter,
        //     training: res.id,
        //     type: this.selectedType,
        //   })
        //   .subscribe(
        //     (r) => {
        //       console.log(r);
        this.ngOnInit();
        this.modalService.dismissAll();
        //this.translate.instant('HOME.TITLE')
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );

        //   },
        //   (er) => {
        //     if (er.error.detail != null) {
        //       AlertNotif.finish('Nouvel ajout', er.error.detail, 'error');
        //     } else {
        //       AlertNotif.finish(
        //         'Nouvel ajout',
        //         'Erreur, Verifiez que vous avez une bonne connexion internet',
        //         'error'
        //       );
        //     }
        //   }
        // );
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish('Nouvel ajout', err.error.detail, 'error');
        } else {
          AlertNotif.finish(
            'Nouvel ajout',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression definitive',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.trainingService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            this.ngOnInit();
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock(id, index) {
    AlertNotif.finishConfirm(
      'Suspension de la formation',
      ' Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.trainingService.delete(id).subscribe(
          (res: any) => {
            // this.data.splice(index,1)
            this.ngOnInit();
            AlertNotif.finish(
              'Suspension',
              'SuppreSuspensionssion effectuée avec succès',
              'success'
            );
          },
          (err) => {
            AlertNotif.finish(
              'Suspension',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  edit(value) {
    const val = { name: value.trainingName };
    this.trainingService.update_partial(val, value.id).subscribe(
      (res) => {
        this.ngOnInit();
        AlertNotif.finish(
          'Nouvelle modification',
          'Motification effectué avec succès',
          'success'
        );
      },
      (err) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }
}

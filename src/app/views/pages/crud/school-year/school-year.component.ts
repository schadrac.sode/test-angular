import { Component, OnInit, Input } from '@angular/core';
// import { PipeTransform } from '@angular/core';
// import { DecimalPipe } from '@angular/common';
// import { FormControl } from '@angular/forms';
// import { NgForm } from '@angular/forms';

import * as moment from 'moment';

// import { Observable } from 'rxjs';
// import { map, startWith } from 'rxjs/operators';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbModal,
  NgbDate,
  NgbDateParserFormatter,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { AlertNotif } from 'src/app/alert';
import { TranslateService } from '@ngx-translate/core';

import { SchoolYear } from 'src/app/core/_models/school-year.model';
import { SchoolYearService } from 'src/app/core/_services/school-year.service';

@Component({
  selector: 'app-school-year',
  templateUrl: './school-year.component.html',
  styleUrls: ['./school-year.component.css'],
})
export class SchoolYearComponent implements OnInit {
  @Input() cssClasses = '';
  model: NgbDateStruct;
  date: { year: number; month: number };

  page = 1;
  pageSize = 4;
  searchText = '';
  closeResult = '';
  error = '';
  data: any[] = [];
  _temp: any[] = [];
  collectionSize = 0;
  selected_data = new SchoolYear();
  dateObj: number = Date.now();

  search() {
    this.data = this._temp.filter((r) => {
      const term = this.searchText.toLowerCase();
      return (
        r.start.toString().toLowerCase().includes(term) ||
        r.end.toString().toLowerCase().includes(term)
      );
    });
    this.collectionSize = this.data.length;
  }

  filtter(event) {
    console.log(event.target.value.toString().toLowerCase().toLowerCase());
    this.data = this._temp.filter((el) => {
      const term = event.target.value.toString().toLowerCase();
      return el.deleted.toString().toLowerCase().includes(term);
    });
    //console.log(this.data);
    this.collectionSize = this.data.length;
  }

  openAddModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openEditModal(content, el) {
    //////console.log(el)
    //////console.log(content)
    //this.selected_data;
    //  ////console.log(this.selected_data);
    this.selected_data = el;
    ////console.log(this.selected_data);
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  minDate;
  maxDate;
  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null;
  toDate: NgbDate;

  constructor(
    private modalService: NgbModal,
    private schoolYearService: SchoolYearService,
    private router: Router,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter
  ) {
    this.minDate = 1960;
    this.maxDate = 3000;
    ////console.log(this.minDate);

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (
      this.fromDate &&
      !this.toDate &&
      date &&
      date.after(this.fromDate)
    ) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return (
      this.fromDate &&
      !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.hoveredDate)
    );
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      (this.toDate && date.equals(this.toDate)) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed))
      ? NgbDate.from(parsed)
      : currentValue;
  }

  ngOnInit() {
    this._temp = [];
    this.data = [];
    this.schoolYearService.getAll().subscribe((res: any) => {
      ////console.log(res);
      this.spinner.hide();
      this.data = res;
      this._temp = this.data;
      this.collectionSize = this.data.length;
    });
  }

  toTimestamp(year, month, day) {
    var datum = new Date(Date.UTC(year, month - 1, day));
    return datum.getTime() / 1000;
  }

  create(value) {
    let _start = value.startDate;
    let _end = value.endDate;
    ////console.log([_start, _end]);
    let ressource = {
      start: _start,
      end: _end,
    };
    ////console.log(ressource);
    this.schoolYearService.create(ressource).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvel ajout',
          'Ajout effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },

      (err) => {
        AlertNotif.finish(
          'Nouvel ajout',
          'Erreur, Verifiez que vous avez une bonne connexion internet',
          'error'
        );
      }
    );
  }

  archive(id, index) {
    AlertNotif.finishConfirm(
      'Suppression',
      'Cette action est irreversible. Voulez-vous continuer ?'
    ).then((result) => {
      if (result.value) {
        this.schoolYearService.delete(id).subscribe(
          (res: any) => {
            this.data.splice(index, 1);
            AlertNotif.finish(
              'Suppression',
              'Suppression effectuée avec succès',
              'success'
            );
            this.ngOnInit();
          },
          (err) => {
            AlertNotif.finish(
              'Suppression',
              'Erreur, Verifiez que vous avez une bonne connexion internet',
              'error'
            );
          }
        );
      }
    });
  }

  lock_schoolYear(id, index) {
    AlertNotif.finishConfirm('Suspension', ' Voulez-vous continuer ?').then(
      (result) => {
        if (result.value) {
          this.schoolYearService.delete_partial(id).subscribe(
            (res: any) => {
              // this.data.splice(index,1)
              AlertNotif.finish(
                'Suspension',
                'Suspension effectuée avec succès',
                'success'
              );
              this.ngOnInit();
            },
            (err) => {
              if (err.error.detail != null) {
                AlertNotif.finish('Suspension', err.error.detail, 'error');
              } else {
                AlertNotif.finish(
                  'Suspension',
                  'Erreur, Verifiez que vous avez une bonne connexion internet',
                  'error'
                );
              }
            }
          );
        }
      }
    );
  }

  edit(value) {
    let _start = value.startDate;
    let _end = value.endDate;
    ////console.log([_start, _end]);
    let ressource = {
      start: _start,
      end: _end,
    };
    ////console.log(ressource);

    this.schoolYearService.update_partial(ressource, value.id).subscribe(
      (res) => {
        AlertNotif.finish(
          'Nouvelle modification',
          'modification effectué avec succès',
          'success'
        );
        this.ngOnInit();
      },
      (err) => {
        if (err.error.detail != null) {
          AlertNotif.finish(
            'Nouvelle  modification',
            err.error.detail,
            'error'
          );
        } else {
          AlertNotif.finish(
            'Nouvelle modification',
            'Erreur, Verifiez que vous avez une bonne connexion internet',
            'error'
          );
        }
      }
    );
  }
}

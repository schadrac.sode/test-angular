import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthentificationService } from 'src/app/core/_services/authentification.service';
import { LocalService } from 'src/app/core/_services/browser-storages/local.service';
import { TranslateService } from '@ngx-translate/core';
import { Roles } from 'src/app/core/_models/roles';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  returnUrl = '';
  loading = false;
  error = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private localStorageService: LocalService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthentificationService
  ) {}
  lang = 'fr';
  ngOnInit(): void {
    this.lang = this.activatedRoute.snapshot.paramMap.get('lang');
    if (this.lang === 'fr' || this.lang === 'en') {
      this.translateService.use(this.lang);
    } else {
      this.router.navigateByUrl('/login');
    }
    if (this.lang === null) {
      this.lang = 'fr';
      this.translateService.use('fr');
    }
    this.route.queryParams.subscribe((params) => {
      this.returnUrl = params.returnUrl || '/dashboard';
    });
  }

  submit(value) {
		this.loading = true;
		this.auth
			.login(value.email, value.password)
			.subscribe((res:any) => {
				console.log(res)
        this.loading = false;
				if (res) {
					localStorage.setItem('seweToken',res.token);
					// localStorage.setItem('seweUserRole',res.data.user.role);
					this.localStorageService.setJsonValue('seweUserData',res.user)
        //	this.router.navigateByUrl(this.returnUrl);
          window.location.reload()
				}

			},err => {
				console.log(err)
				this.loading = false;
				if(err.error.non_field_errors!=null){
					this.error=err.error.non_field_errors[0]
				}
				});
	}

}

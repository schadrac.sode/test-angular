import { Component, OnInit,Input } from '@angular/core';
import { LocalService } from 'src/app/core/_services/browser-storages/local.service';
import { Router } from '@angular/router';
import { Roles } from 'src/app/core/_models/roles';
import { UserService } from 'src/app/core/_services/user.service';
import { User } from 'src/app/core/_models/user.model';
import { AlertNotif } from 'src/app/alert';

@Component({
  selector: 'kt-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  @Input() cssClasses = '';
  error = '';
  current_role = '';
  user = new  User();
  file: File;
  urls;

  constructor(
    private localService: LocalService,
    private userService: UserService,
    private router: Router,
    private localStorageService: LocalService) { }

  ngOnInit(): void {
      // this.current_role=localStorage.getItem('seweUserRole')
      this.user = new User(this.localStorageService.getJsonValue('seweUserData'));
      console.log(this.user);
  }

  resetPassword(value){
    if (value.new_password === value.new_password_conf){
      this.userService.set_password(value, this.user.id).subscribe((res:any) => {
        this.localService.setJsonValue('seweUserData', res);
        AlertNotif.finish('Modification de mot de passe', 'Votre mise à jour de mot de passe été prise en compte avec succès. A présent nous vous déconnecterons et vous reconnecterez avec votre nouveau mot de passe.', 'success')
        this.signout();
      }, (err) => {
        AlertNotif.finish('Modification de mot de passe', 'Une erreur est survenue, verifier votre connexion internet puis reessayer', 'error')
      });
    }
  }
  signout(){
    localStorage.removeItem('seweToken');
    // localStorage.removeItem('seweUserRole')
    this.localStorageService.clearToken();
    this.router.navigateByUrl('/login');
  }

  updateUser(value){
    console.log(value);

    const formData = new FormData();
    // formData.append("username",value.last_name+" "+value.first_name )
    formData.append('last_name', value.last_name);
    formData.append('phone', value.phone);
    formData.append('email', this.user.email);
    formData.append('first_name', value.first_name);

    if (this.file){
      formData.append('profil_image', this.file);
    }
    this.userService.update_partial(formData, this.user.id).subscribe((res:any) => {
      this.localService.setJsonValue('seweUserData', res);
      AlertNotif.finish('Modification de profil', 'Votre mise à jour de profil été prise en compte avec succès', 'success')

      this.ngOnInit();
    }, (err) => {
      AlertNotif.finish('Modification de profil', 'Une erreur est survenue, verifier votre connexion internet puis reessayer', 'error')
    });
  }
  // onFileChange(event) {
  //   this.file=null
  //   if (event.target.files.length > 0) {
  //     this.file = event.target.files[0];
  //   //  this.form.get('avatar').setValue(file);
  //   }
  // }


  removeLogo() {
    this.file = undefined;
    this.urls = undefined;
  }

  onFileChange(event) {
    // console.log(event.target.files);
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }

    // console.log(this.file);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.urls = event.target;
        console.log([event.target, reader.result]);
      };
    }
  }

}

import { SubscriptionService } from './../../../core/_services/subscription.service';
import { BuyTrainingService } from './../../../core/_services/buy-training.service';
import { BuyBookService } from './../../../core/_services/buy-book.service';
import { UserService } from './../../../core/_services/user.service';
import { Component, OnInit } from '@angular/core';
import { Color, Label, MultiDataSet } from 'ng2-charts';
import { ChartDataSets, ChartType } from 'chart.js';
import { Roles } from 'src/app/core/_models/roles';
import { User } from 'src/app/core/_models/user.model';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  lineChartLabels: Label[] = [
    'Jan',
    'Fev',
    'Mar',
    'Avr',
    'Mai',
    'Juin',
    'Juil',
    'Août',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      // backgroundColor: 'transparent',
    },
    {
      borderColor: 'blue',
      // backgroundColor: 'white',
    },
    {
      borderColor: 'pink',
      // backgroundColor: 'white',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  //
  doughnutChartLabels: Label[] = [
    'Abonnement actif',
    'Abonnement expirer',
    'Abonnement à valider',
  ];
  doughnutChartData: MultiDataSet = [[0, 0, 0]];

  doughnutChartType: ChartType = 'doughnut';

  //
  current_role = '';

  userNUmber = [];
  buyBookNumber = [];
  buyTrainingNumberO = [];
  buyTrainingNumberP = [];
  subs = [];
  subActive = [];
  subValidate = [];
  subExpired = [];
  cUPoptions;
  buyBookJan = [];
  buyBookFev = [];
  buyBookMars = [];
  buyBookAvl = [];
  buyBookMai = [];
  buyBookJun = [];
  buyBookJui = [];
  buyBookAout = [];
  buyBookSep = [];
  buyBookOct = [];
  buyBookNov = [];
  buyBookDec = [];

  buyTrainPJan = [];
  buyTrainPFev = [];
  buyTrainPMars = [];
  buyTrainPAvl = [];
  buyTrainPMai = [];
  buyTrainPJun = [];
  buyTrainPJui = [];
  buyTrainPAout = [];
  buyTrainPSep = [];
  buyTrainPOct = [];
  buyTrainPNov = [];
  buyTrainPDec = [];

  buyTrainEJan = [];
  buyTrainEFev = [];
  buyTrainEMars = [];
  buyTrainEAvl = [];
  buyTrainEMai = [];
  buyTrainEJun = [];
  buyTrainEJui = [];
  buyTrainEAout = [];
  buyTrainESep = [];
  buyTrainEOct = [];
  buyTrainENov = [];
  buyTrainEDec = [];

  yearItems = [
    { id: 2017, name: '2017' },
    { id: 2018, name: '2018' },
    { id: 2019, name: '2019' },
    { id: 2020, name: '2020' },
  ];
  selectedyearItem: any;

  lineChartData: ChartDataSets[] = [
    {
      data: [
        this.buyBookJan.length,
        this.buyBookFev.length,
        this.buyBookMars.length,
        this.buyBookAvl.length,
        this.buyBookMai.length,
        this.buyBookJun.length,
        this.buyBookJui.length,
        this.buyBookAout.length,
        this.buyBookSep.length,
        this.buyBookOct.length,
        this.buyBookNov.length,
        this.buyBookDec.length,
      ],
      label: "Nbre d'achat de livre par mois",
    },
    {
      data: [
        this.buyTrainPJan.length,
        this.buyTrainPFev.length,
        this.buyTrainPMars.length,
        this.buyTrainPAvl.length,
        this.buyTrainPMai.length,
        this.buyTrainPJun.length,
        this.buyTrainPJui.length,
        this.buyTrainPAout.length,
        this.buyTrainPSep.length,
        this.buyTrainPOct.length,
        this.buyTrainPNov.length,
        this.buyTrainPDec.length,
      ],
      label: "Nbre d'achat de Formation présentielle par mois",
    },
    {
      data: [
        this.buyTrainEJan.length,
        this.buyTrainEFev.length,
        this.buyTrainEMars.length,
        this.buyTrainEAvl.length,
        this.buyTrainEMai.length,
        this.buyTrainEJun.length,
        this.buyTrainEJui.length,
        this.buyTrainEAout.length,
        this.buyTrainESep.length,
        this.buyTrainEOct.length,
        this.buyTrainENov.length,
        this.buyTrainEDec.length,
      ],
      label: "Nbre d'achat de Formation online par mois",
    },
  ];

  i = 0;
  j = 0;
  k = 0;
  l = 0;
  m = 0;
  n = 0;
  o = 0;
  p = 0;
  q = 0;
  r = 0;
  s = 0;
  t = 0;

  constructor(
    private userService: UserService,
    private buyBookService: BuyBookService,
    private buyTrainingService: BuyTrainingService,
    private subcriptionService: SubscriptionService
  ) {
    this.cUPoptions = {
      startVal: 0,
      duration: 5,
    };

    this.selectedyearItem = moment().year();
  }

  ngOnInit(): void {
    this.userService.getAll().subscribe((user) => {
      this.userNUmber = [];
      user.forEach((e) => {
        if (e.role === Roles.User) {
          this.userNUmber.push(new User(e));
        }
      });
    });
    this.buyBookService.getAll().subscribe((book) => {
      this.buyBookNumber = [];
      this.buyBookAout = [];
      this.buyBookJan = [];
      this.buyBookJui = [];
      this.buyBookJun = [];
      this.buyBookFev = [];
      this.buyBookMai = [];
      this.buyBookMars = [];
      this.buyBookSep = [];
      this.buyBookDec = [];
      this.buyBookAvl = [];
      this.buyBookNov = [];
      this.buyBookOct = [];

      book.forEach((b) => {
        const year = moment(b.createdAt).year();
        if (year === this.selectedyearItem) {
          this.buyBookNumber.push(b);
        }
      });
      console.log(this.buyBookNumber);
      this.buyBookNumber.forEach((bk) => {
        if (this.verifyMonth(1, bk.createdAt)) {
          this.buyBookJan.push(bk);
        }
        if (this.verifyMonth(2, bk.createdAt)) {
          this.buyBookFev.push(bk);
        }
        if (this.verifyMonth(3, bk.createdAt)) {
          this.buyBookMars.push(bk);
        }
        if (this.verifyMonth(4, bk.createdAt)) {
          this.buyBookAvl.push(bk);
        }
        if (this.verifyMonth(5, bk.createdAt)) {
          this.buyBookMai.push(bk);
        }
        if (this.verifyMonth(6, bk.createdAt)) {
          this.buyBookJun.push(bk);
        }
        if (this.verifyMonth(7, bk.createdAt)) {
          this.buyBookJui.push(bk);
        }
        if (this.verifyMonth(8, bk.createdAt)) {
          this.buyBookAout.push(bk);
        }
        if (this.verifyMonth(9, bk.createdAt)) {
          this.buyBookSep.push(bk);
        }
        if (this.verifyMonth(10, bk.createdAt)) {
          this.buyBookOct.push(bk);
        }
        if (this.verifyMonth(11, bk.createdAt)) {
          this.buyBookNov.push(bk);
        }
        if (this.verifyMonth(12, bk.createdAt)) {
          this.buyBookDec.push(bk);
        }
      });
    });
    this.buyTrainingService.getAll().subscribe((res) => {
      this.buyTrainingNumberO = [];
      this.buyTrainingNumberP = [];
      this.buyTrainPJan = [];
      this.buyTrainPFev = [];
      this.buyTrainPMars = [];
      this.buyTrainPAvl = [];
      this.buyTrainPMai = [];
      this.buyTrainPJun = [];
      this.buyTrainPJui = [];
      this.buyTrainPAout = [];
      this.buyTrainPSep = [];
      this.buyTrainPOct = [];
      this.buyTrainPNov = [];
      this.buyTrainPDec = [];

      this.buyTrainEJan = [];
      this.buyTrainEFev = [];
      this.buyTrainEMars = [];
      this.buyTrainEAvl = [];
      this.buyTrainEMai = [];
      this.buyTrainEJun = [];
      this.buyTrainEJui = [];
      this.buyTrainEAout = [];
      this.buyTrainESep = [];
      this.buyTrainEOct = [];
      this.buyTrainENov = [];
      this.buyTrainEDec = [];
      res.forEach((t) => {
        const year = moment(t.createdAt).year();
        if (
          year === this.selectedyearItem &&
          t.trainingCenter.type === 'presentiel'
        ) {
          this.buyTrainingNumberP.push(t);
        } else if (
          year === this.selectedyearItem &&
          t.trainingCenter.type === 'numerique'
        ) {
          this.buyTrainingNumberO.push(t);
        }
      });
      this.buyTrainingNumberP.forEach((tr) => {
        if (this.verifyMonth(1, tr.createdAt)) {
          this.buyTrainPJan.push(tr);
        }
        if (this.verifyMonth(2, tr.createdAt)) {
          this.buyTrainPFev.push(tr);
        }
        if (this.verifyMonth(3, tr.createdAt)) {
          this.buyTrainPMars.push(tr);
        }
        if (this.verifyMonth(4, tr.createdAt)) {
          this.buyTrainPAvl.push(tr);
        }
        if (this.verifyMonth(5, tr.createdAt)) {
          this.buyTrainPMai.push(tr);
        }
        if (this.verifyMonth(6, tr.createdAt)) {
          this.buyTrainPJun.push(tr);
        }
        if (this.verifyMonth(7, tr.createdAt)) {
          this.buyTrainPJui.push(tr);
        }
        if (this.verifyMonth(8, tr.createdAt)) {
          this.buyTrainPAout.push(tr);
        }
        if (this.verifyMonth(9, tr.createdAt)) {
          this.buyTrainPSep.push(tr);
        }
        if (this.verifyMonth(10, tr.createdAt)) {
          this.buyTrainPOct.push(tr);
        }
        if (this.verifyMonth(11, tr.createdAt)) {
          this.buyTrainPNov.push(tr);
        }
        if (this.verifyMonth(12, tr.createdAt)) {
          this.buyTrainPDec.push(tr);
        }
      });
      this.buyTrainingNumberO.forEach((tr) => {
        if (this.verifyMonth(1, tr.createdAt)) {
          this.buyTrainEJan.push(tr);
        }
        if (this.verifyMonth(2, tr.createdAt)) {
          this.buyTrainEFev.push(tr);
        }
        if (this.verifyMonth(3, tr.createdAt)) {
          this.buyTrainEMars.push(tr);
        }
        if (this.verifyMonth(4, tr.createdAt)) {
          this.buyTrainEAvl.push(tr);
        }
        if (this.verifyMonth(5, tr.createdAt)) {
          this.buyTrainEMai.push(tr);
        }
        if (this.verifyMonth(6, tr.createdAt)) {
          this.buyTrainEJun.push(tr);
        }
        if (this.verifyMonth(7, tr.createdAt)) {
          this.buyTrainEJui.push(tr);
        }
        if (this.verifyMonth(8, tr.createdAt)) {
          this.buyTrainEAout.push(tr);
        }
        if (this.verifyMonth(9, tr.createdAt)) {
          this.buyTrainESep.push(tr);
        }
        if (this.verifyMonth(10, tr.createdAt)) {
          this.buyTrainEOct.push(tr);
        }
        if (this.verifyMonth(11, tr.createdAt)) {
          this.buyTrainENov.push(tr);
        }
        if (this.verifyMonth(12, tr.createdAt)) {
          this.buyTrainEDec.push(tr);
        }
      });
      this.lineChartData = [
        {
          data: [
            this.buyBookJan.length,
            this.buyBookFev.length,
            this.buyBookMars.length,
            this.buyBookAvl.length,
            this.buyBookMai.length,
            this.buyBookJun.length,
            this.buyBookJui.length,
            this.buyBookAout.length,
            this.buyBookSep.length,
            this.buyBookOct.length,
            this.buyBookNov.length,
            this.buyBookDec.length,
          ],
          label: "Nbre d'achat de livre par mois",
        },
        {
          data: [
            this.buyTrainPJan.length,
            this.buyTrainPFev.length,
            this.buyTrainPMars.length,
            this.buyTrainPAvl.length,
            this.buyTrainPMai.length,
            this.buyTrainPJun.length,
            this.buyTrainPJui.length,
            this.buyTrainPAout.length,
            this.buyTrainPSep.length,
            this.buyTrainPOct.length,
            this.buyTrainPNov.length,
            this.buyTrainPDec.length,
          ],
          label: "Nbre d'achat de Formation présentielle par mois",
        },
        {
          data: [
            this.buyTrainEJan.length,
            this.buyTrainEFev.length,
            this.buyTrainEMars.length,
            this.buyTrainEAvl.length,
            this.buyTrainEMai.length,
            this.buyTrainEJun.length,
            this.buyTrainEJui.length,
            this.buyTrainEAout.length,
            this.buyTrainESep.length,
            this.buyTrainEOct.length,
            this.buyTrainENov.length,
            this.buyTrainEDec.length,
          ],
          label: "Nbre d'achat de Formation online par mois",
        },
      ];
    });

    this.subcriptionService.getAll().subscribe((services) => {
      this.subs = services;
      this.subActive = [];
      this.subValidate = [];
      this.subExpired = [];
      services.forEach((t) => {
        const d = moment(t.expiredAt);
        const td = moment();
        if (t.is_active) {
          this.subActive.push(t);
        }
        if (t.validate === 'validate') {
          this.subValidate.push(t);
        }
        if (t.validate === 'validate' && d.diff(td, 'days') < 0) {
          this.subExpired.push(t);
        }
      });
      const atv = (this.subActive.length * 100) / this.subs.length;
      const vlt = (this.subValidate.length * 100) / this.subs.length;
      const exp = (this.subExpired.length * 100) / this.subs.length;
      // console.log([atv, exp, vlt]);
      this.doughnutChartData = [[atv, exp, vlt]];
    });
  }
  change(event) {
    console.log(event);
    this.selectedyearItem = event.id;
    this.ngOnInit();
  }

  verifyMonth(vmonth, datemonth): boolean {
    const f = moment(datemonth).month();
    if (vmonth === f) {
      return true;
    } else {
      return false;
    }
  }
}

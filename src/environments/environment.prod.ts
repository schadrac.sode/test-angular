export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	API_SCHEME: 'https',
	API_DOMAIN: 'api.se-we.com/api',
	API_FILE: 'api.se-we.com',
	API_VERSION: 'v1'

};
